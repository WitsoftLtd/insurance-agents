import { MaterialModule } from './modules/material/material.module';
import { BrowserModule, Title, Meta } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { MenuComponent } from './site/layouts/menu/menu.component';
import { LandingPageComponent } from './site/landing-page/landing-page.component';
import { FooterComponent } from './site/layouts/footer/footer.component';
import { RegisterComponent } from './site/pages/register/register.component';
import { LoginComponent } from './site/pages/login/login.component';
import { ConfirmEmailComponent } from './site/pages/confirm-email/confirm-email.component';
import { JwtInterceptor } from './shared/services/jwt.interceptor';
import { ForgotPasswordComponent } from './site/pages/forgot-password/forgot-password.component';
import { HomeComponent } from './app/home/home.component';
import { DashboardComponent } from './app/dashboard/dashboard.component';
import { LayoutModule } from '@angular/cdk/layout';
import { NavigationComponent } from './app/layout/navigation/navigation.component';
import { SidenavComponent } from './app/layout/sidenav/sidenav.component';
import { ToolbarComponent } from './app/layout/toolbar/toolbar.component';
import { CompaniesComponent } from './app/pages/companies/companies.component';
import { CompanyComponent } from './app/pages/companies/company/company.component';
import { ConfirmDialogComponent } from './shared/confirm-dialog/confirm-dialog.component';
import { ProductsListComponent } from './app/pages/products/products-list/products-list.component';
import { ProductComponent } from './app/pages/products/product/product.component';
import { ClientsListComponent } from './app/pages/clients/clients-list/clients-list.component';
import { ClientComponent } from './app/pages/clients/client/client.component';
import { AgentComponent } from './app/pages/agents/agent/agent.component';
import { AgentsListComponent } from './app/pages/agents/agents-list/agents-list.component';
import { PoliciesListComponent } from './app/pages/policies/policies-list/policies-list.component';
import { PolicyComponent } from './app/pages/policies/policy/policy.component';
import { ReportsListComponent } from './app/pages/reports/reports-list/reports-list.component';
import { WorkspaceComponent } from './app/pages/workspaces/workspace/workspace.component';
import { SmsTestComponent } from './app/pages/workspaces/sms-test/sms-test.component';
import { EmailTestComponent } from './app/pages/workspaces/email-test/email-test.component';

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    MenuComponent,
    LandingPageComponent,
    FooterComponent,
    RegisterComponent,
    LoginComponent,
    ConfirmEmailComponent,
    ForgotPasswordComponent,
    HomeComponent,
    DashboardComponent,
    NavigationComponent,
    SidenavComponent,
    ToolbarComponent,
    CompaniesComponent,
    CompanyComponent,
    ConfirmDialogComponent,
    ProductsListComponent,
    ProductComponent,
    ClientsListComponent,
    ClientComponent,
    AgentComponent,
    AgentsListComponent,
    PoliciesListComponent,
    PolicyComponent,
    ReportsListComponent,
    WorkspaceComponent,
    SmsTestComponent,
    EmailTestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MaterialModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    Title,
    Meta
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmDialogComponent,
    CompanyComponent
  ]
})
export class AppModule { }
