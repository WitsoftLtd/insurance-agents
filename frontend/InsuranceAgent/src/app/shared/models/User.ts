export interface User {
  id: number;
  email: string;
  fullName: string;
  phoneNumber: string;
  tokenString: string;
  defaultWorkSpace: number;
  workSpaces: Array<any>;
}
