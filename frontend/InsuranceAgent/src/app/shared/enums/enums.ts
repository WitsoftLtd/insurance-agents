export enum ClientType {
  Individual,
  Company
}

export enum ClientStatus {
  Client = 1,
  Prospect,
  FormerClients,
}

export enum Gender {
  Female,
  Male,
  Other
}

export enum MaritalStatus {
  Single,
  Married,
  DomesticPartnership,
  Divorced,
  Widowed
}

export enum PolicyStatus {
  PreActive,
  Active,
  History,
  Canceled
}

export enum PaymentFrequency {
  Monthly,
  Bi_Monthly,
  Quartely,
  Half_Yearly,
  Annual
}

export enum PaymentMethod {
  Cash,
  MobileMoney,
  Debit_Credit_Card,
  StandingOrder,
  Payroll,
  Other
}
