import { ReportsListComponent } from './app/pages/reports/reports-list/reports-list.component';
import { PoliciesListComponent } from './app/pages/policies/policies-list/policies-list.component';
import { AgentsListComponent } from './app/pages/agents/agents-list/agents-list.component';
import { ClientsListComponent } from './app/pages/clients/clients-list/clients-list.component';
import { ProductsListComponent } from './app/pages/products/products-list/products-list.component';
import { CompaniesComponent } from './app/pages/companies/companies.component';
import { HomeComponent } from './app/home/home.component';
import { ForgotPasswordComponent } from './site/pages/forgot-password/forgot-password.component';
import { ConfirmEmailComponent } from './site/pages/confirm-email/confirm-email.component';
import { LoginComponent } from './site/pages/login/login.component';
import { RegisterComponent } from './site/pages/register/register.component';
import { LandingPageComponent } from './site/landing-page/landing-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/services/auth.guard';


const routes: Routes = [
  {path: 'home', component: LandingPageComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'confirmEmail', component: ConfirmEmailComponent},
  {path: 'forgotPassowrd', component: ForgotPasswordComponent},
  {path: 'app', component: HomeComponent, canActivate: [AuthGuard] ,
    children: [
      {path: 'companies', component: CompaniesComponent},
      {path: 'products', component: ProductsListComponent},
      {path: 'clients', component: ClientsListComponent},
      {path: 'agents', component: AgentsListComponent},
      {path: 'policies', component: PoliciesListComponent},
      {path: 'reports', component: ReportsListComponent}
    ]},

  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
