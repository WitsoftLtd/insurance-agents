import { Injectable } from '@angular/core';
import { IAgent } from '../models/agent';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/site/helpers/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AgentsService {

  private dataSourceSubject = new BehaviorSubject<IAgent[]>([]);
  dataSource = this.dataSourceSubject.asObservable();

  wasFetched: boolean;

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    applicationUserId: new FormControl(null),
    fullName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl(''),
    canLogIn: new FormControl(false)
  });
  constructor(private http: HttpClient,
              private constants: Constants) { }

  // const params = new HttpParams()
  //   .set('workSpaceId', workSpaceId.toString());

  initializeFormGroup() {
    this.form.setValue({
      id: 0,
      applicationUserId: 0,
      email: '',
      fullName: '',
      phoneNumber: '',
      canLogIn: false
    });
  }

  populateForm(model: IAgent) {
    this.form.setValue({
      id: model.id,
      applicationUserId: model.applicationUserId,
      email: model.email,
      fullName: model.fullName,
      phoneNumber: model.phoneNumber,
      canLogIn: model.canLogIn
    });
  }

  onGet(workSpaceId: number) {
    this.http.get<IAgent[]>(`${this.constants.baseUrl}Agents/${workSpaceId}`)
      .subscribe(
        res => {
          this.dataSourceSubject.next(res);
          this.wasFetched = true;
        }
      );
  }

  onCreate(workSpaceId: number, model: IAgent) {
    return this.http.post<IAgent>(`${this.constants.baseUrl}Agents/${workSpaceId}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.next(this.dataSourceSubject.getValue().concat([res]));
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onEdit(model: IAgent) {
    return this.http.put<IAgent>(`${this.constants.baseUrl}Agents/${model.id}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.value.find(e => e.id === res.id).applicationUserId = res.applicationUserId;
          this.dataSourceSubject.value.find(e => e.id === res.id).fullName = res.fullName;
          this.dataSourceSubject.value.find(e => e.id === res.id).email = res.email;
          this.dataSourceSubject.value.find(e => e.id === res.id).phoneNumber = res.phoneNumber;
          this.dataSourceSubject.value.find(e => e.id === res.id).canLogIn = res.canLogIn;
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onDelete(id: number) {
    return this.http.delete<number>(`${this.constants.baseUrl}Agents/${id}`)
    .pipe(
      map(res => {
        const index = this.dataSourceSubject.value.findIndex(e => e.id === res);
        if (index > -1) {
          this.dataSourceSubject.value.splice(index, 1);
          this.dataSourceSubject.next(this.dataSourceSubject.value);
        }
        return res;
      })
    );
  }
}
