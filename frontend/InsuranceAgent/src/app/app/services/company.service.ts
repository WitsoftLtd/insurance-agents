import { ICompany } from './../models/company';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Constants } from 'src/app/site/helpers/constants';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private dataSourceSubject = new BehaviorSubject<ICompany[]>([]);
  dataSource = this.dataSourceSubject.asObservable();

  hasServerError: boolean;
  serverError: string;
  wasFetched: boolean;

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    name: new FormControl('', Validators.required),
    contactPerson: new FormControl(''),
    contactPersonPhone: new FormControl(''),
    email: new FormControl('', Validators.email),
    address: new FormControl(''),
    webAddress: new FormControl(''),
    details: new FormControl(''),
  });
  constructor(private http: HttpClient,
              private constants: Constants) { }

  // const params = new HttpParams()
  //   .set('workSpaceId', workSpaceId.toString());

  initializeFormGroup() {
    this.form.setValue({
      id: 0,
      name: '',
      contactPerson: '',
      contactPersonPhone: '',
      email: '',
      address: '',
      webAddress: '',
      details: ''
    });
  }

  populateForm(model: ICompany) {
    this.form.setValue({
      id: model.id,
      name: model.name,
      contactPerson: model.contactPerson,
      contactPersonPhone: model.contactPersonPhone,
      email: model.email,
      address: model.address,
      webAddress: model.webAddress,
      details: model.details
    });
  }

  onGet(workSpaceId: number) {
    this.http.get<ICompany[]>(`${this.constants.baseUrl}InsuranceCompanies/${workSpaceId}`)
      .subscribe(
        res => {
          this.dataSourceSubject.next(res);
          this.wasFetched = true;
        }
      );
  }

  onCreate(workSpaceId: number, model: ICompany) {
    return this.http.post<ICompany>(`${this.constants.baseUrl}InsuranceCompanies/${workSpaceId}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.next(this.dataSourceSubject.getValue().concat([res]));
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onEdit(model: ICompany) {
    return this.http.put<ICompany>(`${this.constants.baseUrl}InsuranceCompanies/${model.id}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.value.find(e => e.id === res.id).address = res.address;
          this.dataSourceSubject.value.find(e => e.id === res.id).contactPerson = res.contactPerson;
          this.dataSourceSubject.value.find(e => e.id === res.id).contactPersonPhone = res.contactPersonPhone;
          this.dataSourceSubject.value.find(e => e.id === res.id).details = res.details;
          this.dataSourceSubject.value.find(e => e.id === res.id).email = res.email;
          this.dataSourceSubject.value.find(e => e.id === res.id).name = res.name;
          this.dataSourceSubject.value.find(e => e.id === res.id).webAddress = res.webAddress;
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onDelete(id: number) {
    return this.http.delete<number>(`${this.constants.baseUrl}InsuranceCompanies/${id}`)
    .pipe(
      map(res => {
        const index = this.dataSourceSubject.value.findIndex(e => e.id === res);
        if (index > -1) {
          this.dataSourceSubject.value.splice(index, 1);
          this.dataSourceSubject.next(this.dataSourceSubject.value);
        }
        return res;
      })
    );
  }
}
