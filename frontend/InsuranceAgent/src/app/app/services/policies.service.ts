import { IPolicy, IPolicyModel } from './../models/policy';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/site/helpers/constants';
import { map } from 'rxjs/operators';
import { ClientsService } from './clients.service';

@Injectable({
  providedIn: 'root'
})
export class PoliciesService {

  private dataSourceSubject = new BehaviorSubject<IPolicy[]>([]);
  dataSource = this.dataSourceSubject.asObservable();

  hasServerError: boolean;
  serverError: string;

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    clientId: new FormControl(''),
    insuranceCompanyId: new FormControl('', Validators.required),
    productId: new FormControl('', Validators.required),
    agentId: new FormControl('', Validators.required),
    policyNumber: new FormControl('', Validators.required),
    effectiveDate: new FormControl(''),
    expiryDate: new FormControl(''),
    termLength: new FormControl(''),
    insuredSummary: new FormControl(''),
    premium: new FormControl(''),
    deductible: new FormControl(''),
    salesCommission: new FormControl(''),
    policyStatus: new FormControl(1),
    paymentFrequency: new FormControl(0),
    paymentMethod: new FormControl(0),
    totalCharged: new FormControl(''),
    advancePayment: new FormControl(''),
    client: new FormControl('', Validators.required)
  });

  constructor(
    private http: HttpClient,
    private constants: Constants,
    private clientsService: ClientsService) {}

  initializeFormGroup() {
    this.form.setValue({
      id: 0,
      clientId: null,
      insuranceCompanyId: null,
      productId: null,
      agentId: null,
      policyNumber: '',
      effectiveDate: new Date((new Date().getTime())),
      expiryDate: new Date((new Date().getTime())),
      termLength: 0,
      insuredSummary: 0,
      premium: 0,
      deductible: 0,
      salesCommission: 0,
      policyStatus: 1,
      paymentFrequency: 0,
      paymentMethod: 0,
      totalCharged: 0,
      advancePayment: 0,
      client: null
    });
  }

  populateForm(model: IPolicyModel) {
    const clientL = this.clientsService.onGetById(model.clientId);

    this.form.setValue({
      id: model.id,
      clientId: model.clientId,
      insuranceCompanyId: model.insuranceCompanyId,
      productId: model.productId,
      agentId: model.agentId,
      policyNumber: model.policyNumber,
      effectiveDate: model.effectiveDate,
      expiryDate: model.expiryDate,
      termLength: model.termLength,
      insuredSummary: model.insuredSummary,
      premium: model.premium,
      deductible: model.deductible,
      salesCommission: model.salesCommission,
      policyStatus: model.policyStatus,
      paymentFrequency: model.paymentFrequency,
      paymentMethod: model.paymentMethod,
      totalCharged: model.totalCharged,
      advancePayment: model.advancePayment,
      client: clientL === undefined ? null : clientL
    });
  }

  onGet(workSpaceId: number) {
    this.http.get<IPolicy[]>(`${this.constants.baseUrl}Policies/${workSpaceId}`)
      .subscribe(
        res => {
          this.dataSourceSubject.next(res);
        }
      );
  }

  onCreate(workSpaceId: number, model: IPolicyModel) {
    model.clientId = model.client.id;
    return this.http.post<IPolicy>(`${this.constants.baseUrl}Policies/${workSpaceId}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.next(this.dataSourceSubject.getValue().concat([res]));
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onEdit(model: IPolicyModel) {
    model.clientId = model.client.id;
    model.client = null;
    return this.http.put<IPolicy>(`${this.constants.baseUrl}Policies/${model.id}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.value.find(e => e.id === res.id).id = res.id;
          this.dataSourceSubject.value.find(e => e.id === res.id).clientId = res.clientId;
          this.dataSourceSubject.value.find(e => e.id === res.id).insuranceCompanyId = res.insuranceCompanyId;
          this.dataSourceSubject.value.find(e => e.id === res.id).productId = res.productId;
          this.dataSourceSubject.value.find(e => e.id === res.id).agentId = res.agentId;
          this.dataSourceSubject.value.find(e => e.id === res.id).policyNumber = res.policyNumber;
          this.dataSourceSubject.value.find(e => e.id === res.id).effectiveDate = res.effectiveDate;
          this.dataSourceSubject.value.find(e => e.id === res.id).expiryDate = res.expiryDate;
          this.dataSourceSubject.value.find(e => e.id === res.id).termLength = res.termLength;
          this.dataSourceSubject.value.find(e => e.id === res.id).insuredSummary = res.insuredSummary;
          this.dataSourceSubject.value.find(e => e.id === res.id).premium = res.premium;
          this.dataSourceSubject.value.find(e => e.id === res.id).deductible = res.deductible;
          this.dataSourceSubject.value.find(e => e.id === res.id).salesCommission = res.salesCommission;
          this.dataSourceSubject.value.find(e => e.id === res.id).policyStatus = res.policyStatus;
          this.dataSourceSubject.value.find(e => e.id === res.id).paymentFrequency = res.paymentFrequency;
          this.dataSourceSubject.value.find(e => e.id === res.id).paymentMethod = res.paymentMethod;
          this.dataSourceSubject.value.find(e => e.id === res.id).totalCharged = res.totalCharged;
          this.dataSourceSubject.value.find(e => e.id === res.id).advancePayment = res.advancePayment;
          this.dataSourceSubject.value.find(e => e.id === res.id).client = res.client;
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onDelete(id: number) {
    return this.http.delete<number>(`${this.constants.baseUrl}Policies/${id}`)
    .pipe(
      map(res => {
        const index = this.dataSourceSubject.value.findIndex(e => e.id === res);
        if (index > -1) {
          this.dataSourceSubject.value.splice(index, 1);
          this.dataSourceSubject.next(this.dataSourceSubject.value);
        }
        return res;
      })
    );
  }
}
