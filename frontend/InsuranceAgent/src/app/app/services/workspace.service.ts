import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { IWorkSpace } from '../models/workspace';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/site/helpers/constants';
import { SmsModel } from '../models/SmsModel';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { User } from 'src/app/shared/models/User';
import { EmailModel } from '../models/EmailModel';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceService {

  private workSpaceSubject = new BehaviorSubject<IWorkSpace>(null);
  workSpace = this.workSpaceSubject.asObservable();
  currentUserValue: User;

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    name: new FormControl('', Validators.required),
    description: new FormControl(''),
    automaticNotifications: new FormControl(''),
    notificationType: new FormControl(0),
    smsTemplate: new FormControl(''),
    emailTemplate: new FormControl(''),
    emailSubject: new FormControl('')
  });

  smsForm: FormGroup = new FormGroup({
    phoneNumber: new FormControl('', Validators.required),
    message: new FormControl(''),
    userId: new FormControl('')
  });

  emailForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    emailSubject: new FormControl(''),
    emailMessage: new FormControl('')
  });

  constructor(private http: HttpClient,
              private constants: Constants,
              private authenticationService: AuthenticationService) {
      this.authenticationService.currentUser.subscribe(
        (res: User) => {
          this.currentUserValue = res;
          this.workSpaceSubject.next(res.workSpaces[0]);
        }
    );
  }

  populateForm() {
    this.form.setValue({
      id: this.workSpaceSubject.value.id,
      name: this.workSpaceSubject.value.name,
      description: this.workSpaceSubject.value.description,
      automaticNotifications: this.workSpaceSubject.value.automaticNotifications,
      notificationType: this.workSpaceSubject.value.notificationType,
      smsTemplate: this.workSpaceSubject.value.smsTemplate,
      emailTemplate: this.workSpaceSubject.value.emailTemplate,
      emailSubject: this.workSpaceSubject.value.emailSubject
    });
  }

  populateSmsForm() {
    this.smsForm.setValue({
      phoneNumber: this.currentUserValue.phoneNumber,
      message: this.workSpaceSubject.value.smsTemplate,
      userId: this.currentUserValue.id
    });
  }

  populateEmailForm() {
    this.emailForm.setValue({
      email: this.currentUserValue.email,
      emailSubject: this.workSpaceSubject.value.emailSubject,
      emailMessage: this.workSpaceSubject.value.emailTemplate
    });
  }



  onEdit(model: IWorkSpace) {
    return this.http.put<IWorkSpace>(`${this.constants.baseUrl}WorkSpaces/${model.id}`, model)
      .pipe(
        map(res => {
          this.workSpaceSubject.value.name = res.name;
          this.workSpaceSubject.value.automaticNotifications = res.automaticNotifications;
          this.workSpaceSubject.value.description = res.description;
          this.workSpaceSubject.value.emailTemplate = res.emailTemplate;
          this.workSpaceSubject.value.id = res.id;
          this.workSpaceSubject.value.notificationType = res.notificationType;
          this.workSpaceSubject.value.smsTemplate = res.smsTemplate;
          this.workSpaceSubject.next(this.workSpaceSubject.value);
          return res;
        })
      );
  }

  onTestSms(model: SmsModel) {
    return this.http.post<boolean>(`${this.constants.baseUrl}WorkSpaces/TestSms/${this.workSpaceSubject.value.id}`, model)
    .pipe(
      map(res => {
        return res;
      })
    );
  }

  onTestEmail(model: EmailModel) {
    return this.http.post<boolean>(`${this.constants.baseUrl}WorkSpaces/SendTestEmail/${this.workSpaceSubject.value.id}`, model)
    .pipe(
      map(res => {
        return res;
      })
    );
  }
}
