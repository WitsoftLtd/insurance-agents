import { IClient } from './../models/client';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/site/helpers/constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  private dataSourceSubject = new BehaviorSubject<IClient[]>([]);
  dataSource = this.dataSourceSubject.asObservable();

  wasFetched: boolean;

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    clientType: new FormControl(0),
    clientStatus: new FormControl(1),
    clientName: new FormControl('', Validators.required),
    clientNumber: new FormControl(''),
    email: new FormControl('', Validators.email),
    phoneNumber: new FormControl(''),
    altPhoneNumber: new FormControl(''),
    address: new FormControl(''),
    pin: new FormControl(''),
    details: new FormControl(''),
    contactPersonName: new FormControl(''),
    contactPersonPhone: new FormControl(''),
    nationalId: new FormControl(''),
    dateofBirth: new FormControl(''),
    gender: new FormControl(0),
    maritalStatus: new FormControl(0),
    emergencyContactName: new FormControl(''),
    emergencyContactPhone: new FormControl('')
  });

  constructor(private http: HttpClient,
              private constants: Constants) { }

  initializeFormGroup() {
    this.form.setValue({
      id: 0,
      clientType: 0,
      clientStatus: 1,
      clientName: '',
      clientNumber: '',
      email: '',
      phoneNumber: '',
      altPhoneNumber: '',
      address: '',
      pin: '',
      details: '',
      contactPersonName: '',
      contactPersonPhone: '',
      nationalId: '',
      dateofBirth: new Date((new Date().getTime())),
      gender: 0,
      maritalStatus: 0,
      emergencyContactName: '',
      emergencyContactPhone: ''
    });
  }

  populateForm(model: IClient) {
    this.form.setValue({
      id: model.id,
      clientType: model.clientType,
      clientStatus: model.clientStatus,
      clientName: model.clientName,
      clientNumber: model.clientNumber,
      email: model.email,
      phoneNumber: model.phoneNumber,
      altPhoneNumber: model.altPhoneNumber,
      address: model.address,
      pin: model.pin,
      details: model.details,
      contactPersonName: model.contactPersonName,
      contactPersonPhone: model.contactPersonPhone,
      nationalId: model.nationalId,
      dateofBirth: model.dateofBirth,
      gender: model.gender,
      maritalStatus: model.maritalStatus,
      emergencyContactName: model.emergencyContactName,
      emergencyContactPhone: model.emergencyContactPhone
    });
  }

  onGet(workSpaceId: number) {
    this.http.get<IClient[]>(`${this.constants.baseUrl}Clients/${workSpaceId}`)
      .subscribe(
        res => {
          this.wasFetched = true;
          this.dataSourceSubject.next(res);
        }
      );
  }

  onGetById(id: number): IClient {
    return this.dataSourceSubject.value.find(e => e.id === id);
  }

  onCreate(workSpaceId: number, model: IClient) {
    return this.http.post<IClient>(`${this.constants.baseUrl}Clients/${workSpaceId}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.next(this.dataSourceSubject.getValue().concat([res]));
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onEdit(model: IClient) {
    return this.http.put<IClient>(`${this.constants.baseUrl}Clients/${model.id}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.value.find(e => e.id === res.id).id = res.id;
          this.dataSourceSubject.value.find(e => e.id === res.id).clientType = res.clientType;
          this.dataSourceSubject.value.find(e => e.id === res.id).clientStatus = res.clientStatus;
          this.dataSourceSubject.value.find(e => e.id === res.id).clientName = res.clientName;
          this.dataSourceSubject.value.find(e => e.id === res.id).clientNumber = res.clientNumber;
          this.dataSourceSubject.value.find(e => e.id === res.id).email = res.email;
          this.dataSourceSubject.value.find(e => e.id === res.id).phoneNumber = res.phoneNumber;
          this.dataSourceSubject.value.find(e => e.id === res.id).altPhoneNumber = res.altPhoneNumber;
          this.dataSourceSubject.value.find(e => e.id === res.id).address = res.address;
          this.dataSourceSubject.value.find(e => e.id === res.id).pin = res.pin;
          this.dataSourceSubject.value.find(e => e.id === res.id).details = res.details;
          this.dataSourceSubject.value.find(e => e.id === res.id).contactPersonName = res.contactPersonName;
          this.dataSourceSubject.value.find(e => e.id === res.id).contactPersonPhone = res.contactPersonPhone;
          this.dataSourceSubject.value.find(e => e.id === res.id).nationalId = res.nationalId;
          this.dataSourceSubject.value.find(e => e.id === res.id).dateofBirth = res.dateofBirth;
          this.dataSourceSubject.value.find(e => e.id === res.id).gender = res.gender;
          this.dataSourceSubject.value.find(e => e.id === res.id).maritalStatus = res.maritalStatus;
          this.dataSourceSubject.value.find(e => e.id === res.id).emergencyContactName = res.emergencyContactName;
          this.dataSourceSubject.value.find(e => e.id === res.id).emergencyContactPhone = res.emergencyContactPhone;
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onDelete(id: number) {
    return this.http.delete<number>(`${this.constants.baseUrl}Clients/${id}`)
    .pipe(
      map(res => {
        const index = this.dataSourceSubject.value.findIndex(e => e.id === res);
        if (index > -1) {
          this.dataSourceSubject.value.splice(index, 1);
          this.dataSourceSubject.next(this.dataSourceSubject.value);
        }
        return res;
      })
    );
  }

  private getGender(g: any): string {
    return '"' + g + '"';
  }

  // private for
}
