import { IProduct } from './../models/product';
import { Injectable } from '@angular/core';
import { Constants } from 'src/app/site/helpers/constants';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private dataSourceSubject = new BehaviorSubject<IProduct[]>([]);
  dataSource = this.dataSourceSubject.asObservable();

  wasFetched = false;

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    name: new FormControl('', Validators.required)
  });
  constructor(private http: HttpClient,
              private constants: Constants) { }

  initializeFormGroup() {
    this.form.setValue({
      id: 0,
      name: ''
    });
  }

  populateForm(model: IProduct) {
    this.form.setValue({
      id: model.id,
      name: model.name
    });
  }

  onGet(workSpaceId: number) {
    this.http.get<IProduct[]>(`${this.constants.baseUrl}Products/${workSpaceId}`)
      .subscribe(
        res => {
          this.dataSourceSubject.next(res);
          this.wasFetched = true;
        }
      );
  }

  onCreate(workSpaceId: number, model: IProduct) {
    return this.http.post<IProduct>(`${this.constants.baseUrl}Products/${workSpaceId}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.next(this.dataSourceSubject.getValue().concat([res]));
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onEdit(model: IProduct) {
    return this.http.put<IProduct>(`${this.constants.baseUrl}Products/${model.id}`, model)
      .pipe(
        map(res => {
          this.dataSourceSubject.value.find(e => e.id === res.id).name = res.name;
          this.dataSourceSubject.next(this.dataSourceSubject.value);
          return res;
        })
      );
  }

  onDelete(id: number) {
    return this.http.delete<number>(`${this.constants.baseUrl}Products/${id}`)
    .pipe(
      map(res => {
        const index = this.dataSourceSubject.value.findIndex(e => e.id === res);
        if (index > -1) {
          this.dataSourceSubject.value.splice(index, 1);
          this.dataSourceSubject.next(this.dataSourceSubject.value);
        }
        return res;
      })
    );
  }
}
