import { IWorkSpace } from './../models/workspace';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { User } from 'src/app/shared/models/User';
import { Title, Meta } from '@angular/platform-browser';
import { Constants } from 'src/app/site/helpers/constants';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import { WorkspaceComponent } from '../pages/workspaces/workspace/workspace.component';
import { WorkspaceService } from '../services/workspace.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  readonly constants = new Constants();
  isHandset$: Observable<boolean> = this.breakpointObserver.observe([
    Breakpoints.Handset,
    Breakpoints.Small
  ])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    currentUserValue: User;
    isMobile: boolean;
  constructor(private breakpointObserver: BreakpointObserver,
              private authenticationService: AuthenticationService,
              private titleService: Title,
              private metaService: Meta,
              private dialog: MatDialog,
              private workspaceService: WorkspaceService) {
    authenticationService.currentUser.subscribe(
      (res: User) => {
        this.currentUserValue = res;
      }
    );
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.constants.applicationName + ' | Easily manage your clients, and their policies');
    // tslint:disable-next-line: max-line-length
    this.metaService.updateTag({name: 'description', content: 'Are you an insurance agent or agencies, easily manage your clients with our cloud solution, you have got the client let us automate the rest.'});
    this.isHandset$.subscribe(
      res => {
        this.isMobile = res;
      }
    );
  }

  logout() {
    this.authenticationService.logout();
  }

  toggleSideNav(drawer: MatSidenav) {
    if (this.isMobile) {
      drawer.toggle();
    }
  }

  onEditWorkspace(){
    this.workspaceService.populateForm();
    this.dialog.open(WorkspaceComponent, {
      disableClose: true,
      autoFocus: true,
      width: '100%',
      position: {top: '10px'},
      panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
    });
  }

}
