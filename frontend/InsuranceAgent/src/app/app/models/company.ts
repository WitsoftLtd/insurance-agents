export interface ICompany {
  id: number;
  name: string;
  contactPerson: string;
  contactPersonPhone: string;
  email: string;
  address: string;
  webAddress: string;
  details: string;
}
