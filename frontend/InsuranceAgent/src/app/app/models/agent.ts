export interface IAgent {
  id: number;
  applicationUserId: number;
  email: string;
  fullName: string;
  phoneNumber: string;
  canLogIn: boolean;
}
