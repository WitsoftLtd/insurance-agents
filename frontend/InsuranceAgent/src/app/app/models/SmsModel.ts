export interface SmsModel {
  phoneNumber: string;
  message: string;
  userId: number;
}
