export interface IProduct {
  id: number;
  name: string;
  productDetails: Array<any>;
}
