import { IClient } from './client';
import { IProduct } from './product';
import { ICompany } from './company';
import { IAgent } from './agent';
import { PolicyStatus, PaymentFrequency, PaymentMethod } from 'src/app/shared/enums/enums';

export class IPolicy {
  id: number;
  clientId: number;
  insuranceCompanyId: number;
  productId: number;
  agentId: number;
  policyNumber: string;
  effectiveDate: Date;
  expiryDate: Date;
  termLength: number;
  insuredSummary: number;
  premium: number;
  deductible: number;
  salesCommission: number;
  policyStatus: PolicyStatus;
  paymentFrequency: PaymentFrequency;
  paymentMethod: PaymentMethod;
  totalCharged: number;
  advancePayment: number;
  client: IClient;
  insuranceCompany: ICompany;
  product: IProduct;
  Agent: IAgent;
  // policyProductDetails: Array<PolicyProductDetail>;
}

export interface IPolicyModel {
  id: number;
  clientId: number;
  insuranceCompanyId: number;
  productId: number;
  agentId: number;
  policyNumber: string;
  effectiveDate: Date;
  expiryDate: Date;
  termLength: number;
  insuredSummary: number;
  premium: number;
  deductible: number;
  salesCommission: number;
  policyStatus: PolicyStatus;
  paymentFrequency: PaymentFrequency;
  paymentMethod: PaymentMethod;
  totalCharged: number;
  advancePayment: number;
  client: IClient;
  // policyProductDetails: Array<PolicyProductDetail>;
}

// export interface PolicyProductDetail {
//   id: number;
//   productDetailId: number;
//   value: string;
//   label: string;
// }
