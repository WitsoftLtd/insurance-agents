export interface IWorkSpace {
  id: number;
  name: string;
  description: string;
  automaticNotifications: boolean;
  notificationType: number;
  smsTemplate: string;
  emailTemplate: string;
  emailSubject: string;
  noofDaysToFirstNotification: number;
  notificationFrequency: number;
}
