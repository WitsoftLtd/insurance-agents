import { ClientType, ClientStatus, Gender, MaritalStatus } from 'src/app/shared/enums/enums';

export interface IClient {
  id: number;
  clientType: ClientType;
  clientStatus: ClientStatus;
  clientName: string;
  clientNumber: string;
  email: string;
  phoneNumber: string;
  altPhoneNumber: string;
  address: string;
  pin: string;
  details: string;
  // Company
  contactPersonName: string;
  contactPersonPhone: string;
  // Individual
  nationalId: string;
  dateofBirth: Date;
  gender: Gender;
  maritalStatus: MaritalStatus;
  emergencyContactName: string;
  emergencyContactPhone: string;
}
