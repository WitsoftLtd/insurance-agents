export interface EmailModel {
  email: string;
  emailSubject: string;
  emailMessage: string;
}
