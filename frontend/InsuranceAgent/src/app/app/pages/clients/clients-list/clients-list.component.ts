import { ClientComponent } from './../client/client.component';
import { ClientsService } from './../../../services/clients.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { IClient } from 'src/app/app/models/client';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { User } from 'src/app/shared/models/User';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { ClientStatus, ClientType } from 'src/app/shared/enums/enums';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {

  listData: MatTableDataSource<IClient>;
  displayedColumns: string[] = ['clientName',  'clientStatus', 'clientType', 'phoneNumber', 'email', 'actions'];
  displayedColumnsMobile: string[] = ['clientName',  'phoneNumber', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  currentUserValue: User;
  private isHandset: Observable<boolean> = this.breakpointObserver.observe([
    Breakpoints.XSmall
  ])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  isMobile: boolean;

  constructor(private clientsService: ClientsService,
              private authenticationService: AuthenticationService,
              private notificationService: NotificationService,
              private dialog: MatDialog,
              private confrimDialog: DialogService,
              private breakpointObserver: BreakpointObserver) {
    this.authenticationService.currentUser.subscribe(
      (res: User) => {
        this.currentUserValue = res;
      }
    );
    this.isHandset.subscribe(
      res => {
        this.isMobile = res;
      }
    );
  }

  ngOnInit(): void {
    this.clientsService.onGet(this.currentUserValue.workSpaces[0].id);
    this.clientsService.dataSource.subscribe(
      res => {
        this.listData = new MatTableDataSource(res);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele !== 'actions' && data[ele] !== null && data[ele].toLowerCase().indexOf(filter) !== -1;
          });
        };
      }
    );
  }

  getDisplayedColumns(): string[] {
    if (this.isMobile) {
      return this.displayedColumnsMobile;
    } else {
      return this.displayedColumns;
    }
  }

  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter() {
    if (this.searchKey !== null) {
      this.listData.filter = this.searchKey.trim().toLowerCase();
    }
  }

  onCreate() {
    this.clientsService.initializeFormGroup();
    this.dialog.open(ClientComponent, {
      disableClose: true,
      autoFocus: true,
      width: '100%',
      position: {top: '10px'},
      panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
    });
  }

  onEdit(row: IClient){
    this.clientsService.populateForm(row);
    this.dialog.open(ClientComponent, {
      disableClose: true,
      autoFocus: true,
      width: '100%',
      position: {top: '10px'},
      panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
    });
  }

  onDelete(rowId: number) {
    this.confrimDialog.openConfirmDialog('Are you sure to delete this record ?')
    .afterClosed().subscribe(res => {
      if (res){
        this.clientsService.onDelete(rowId)
        .subscribe(
          _ => {
            this.notificationService.warn(' Deleted successfully');
          },
          err => {
            this.notificationService.warn(err.error);
          }
        );
      }
    });
  }

  getStatusDescription(status: ClientStatus) {
    switch (status) {
      case ClientStatus.Client:
        return 'Active';
      case ClientStatus.FormerClients:
        return 'Inactive';
      case ClientStatus.Prospect:
        return 'Prospect';
    }
  }

  getTypeDescription(type: ClientType) {
    switch (type) {
      case ClientType.Company:
        return 'Company';
      case ClientType.Individual:
        return 'Individual';
    }
  }

}
