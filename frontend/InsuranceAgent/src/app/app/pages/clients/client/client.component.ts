import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/User';
import { ClientsService } from 'src/app/app/services/clients.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  ClientTypes = [
    {id: 0, name: 'Individual'},
    {id: 1, name: 'Company'}
  ];
  clientType: number;
  ClientStatuses = [
    {id: 1, name: 'Active'},
    {id: 2, name: 'Prospect'},
    {id: 3, name: 'Inactive'}
  ];
  Genders = [
    {id: 0, name: 'Female'},
    {id: 1, name: 'Male'}
  ];
  MaritalStatuses = [
    {id: 0, name: 'Single'},
    {id: 1, name: 'Married'},
    {id: 2, name: 'Domestic Partnership'},
    {id: 3, name: 'Divorced'},
    {id: 4, name: 'Widowed'}
  ];
  currentUserValue: User;

  constructor(public clientsService: ClientsService,
              private notificationService: NotificationService,
              private authenticationService: AuthenticationService,
              private dialogRef: MatDialogRef<ClientComponent>) { }

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe(
      (res: User) => {
        this.currentUserValue = res;
      }
    );
  }

  onSubmit() {
    if (this.clientsService.form.valid) {
      if (this.clientsService.form.get('id').value !== 0) {
          this.clientsService.onEdit(this.clientsService.form.value)
          .subscribe(
            res => {
              this.notificationService.success(' Updated successfully');
              this.onClose();
            },
            err => {
              this.notificationService.warn(err.error);
            }
          );
      } else {
          this.clientsService.onCreate(
            this.currentUserValue.workSpaces[0].id,
            this.clientsService.form.value)
            .subscribe(
              res => {
                this.notificationService.success(' Saved successfully');
                this.onClose();
              },
              err => {
                console.log(err);
                this.notificationService.warn(err.error);
              }
            );
      }
    }
  }

  onClose() {
    this.clientsService.form.reset();
    this.clientsService.initializeFormGroup();
    this.dialogRef.close();
  }

  onClear() {
    this.clientsService.form.reset();
    this.clientsService.initializeFormGroup();
  }
}
