import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/User';
import { ProductsService } from 'src/app/app/services/products.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  currentUserValue: User;

  constructor(public productService: ProductsService,
              private notificationService: NotificationService,
              private authenticationService: AuthenticationService,
              private dialogRef: MatDialogRef<ProductComponent>) { }

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe(
      (res: User) => {
        this.currentUserValue = res;
      }
    );
  }

  onSubmit() {
    if (this.productService.form.valid) {
      if (this.productService.form.get('id').value !== 0) {
          this.productService.onEdit(this.productService.form.value)
          .subscribe(
            res => {
              this.notificationService.success(' Updated successfully');
              this.onClose();
            },
            err => {
              this.notificationService.warn(err.error);
            }
          );
      } else {
          this.productService.onCreate(
            this.currentUserValue.workSpaces[0].id,
            this.productService.form.value)
            .subscribe(
              res => {
                this.notificationService.success(' Saved successfully');
                this.onClose();
              },
              err => {
                console.log(err);
                this.notificationService.warn(err.error);
              }
            );
      }
    }
  }

  onClose() {
    this.productService.form.reset();
    this.productService.initializeFormGroup();
    this.dialogRef.close();
  }

  onClear() {
    this.productService.form.reset();
    this.productService.initializeFormGroup();
  }

}
