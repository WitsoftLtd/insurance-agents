import { ProductsService } from './../../../services/products.service';
import { IProduct } from './../../../models/product';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { User } from 'src/app/shared/models/User';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { ProductComponent } from '../product/product.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  listData: MatTableDataSource<IProduct>;
  displayedColumns: string[] = ['name', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  currentUserValue: User;
  private isHandset: Observable<boolean> = this.breakpointObserver.observe([
    Breakpoints.XSmall
  ])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  isMobile: boolean;

  constructor(private productService: ProductsService,
              private authenticationService: AuthenticationService,
              private notificationService: NotificationService,
              private dialog: MatDialog,
              private confrimDialog: DialogService,
              private breakpointObserver: BreakpointObserver) {
    this.authenticationService.currentUser
      .subscribe(
        (res: User) => {
        this.currentUserValue = res;
      });

    this.isHandset.
      subscribe(
        res => {
        this.isMobile = res;
      });
}

ngOnInit(): void {
  this.productService.onGet(this.currentUserValue.workSpaces[0].id);
  this.productService.dataSource.subscribe(
    res => {
      this.listData = new MatTableDataSource(res);
      this.listData.sort = this.sort;
      this.listData.paginator = this.paginator;
      this.listData.filterPredicate = (data, filter) => {
        return this.displayedColumns.some(ele => {
          return ele !== 'actions' && data[ele] !== null && data[ele].toLowerCase().indexOf(filter) !== -1;
        });
      };
    }
  );
}

onSearchClear() {
  this.searchKey = '';
  this.applyFilter();
}

applyFilter() {
  if (this.searchKey !== null) {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }
}

onCreate() {
  this.productService.initializeFormGroup();
  this.dialog.open(ProductComponent, {
    disableClose: true,
    autoFocus: true,
    width: '100%',
    position: {top: '10px'},
    panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
  });
}

onEdit(row: IProduct){
  this.productService.populateForm(row);
  this.dialog.open(ProductComponent, {
    disableClose: true,
    autoFocus: true,
    width: '100%',
    position: {top: '10px'},
    panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
  });
}

onDelete(rowId: number) {
  this.confrimDialog.openConfirmDialog('Are you sure to delete this record ?')
  .afterClosed().subscribe(res => {
    if (res){
      this.productService.onDelete(rowId)
      .subscribe(
        _ => {
          this.notificationService.warn(' Deleted successfully');
        },
        err => {
          this.notificationService.warn(err.error);
        }
      );
    }
  });
}

}
