import { IClient } from './../../../models/client';
import { Subscription, Observable } from 'rxjs';
import { AgentsService } from './../../../services/agents.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { PoliciesService } from 'src/app/app/services/policies.service';
import { User } from 'src/app/shared/models/User';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { MatDialogRef } from '@angular/material/dialog';
import { ClientsService } from 'src/app/app/services/clients.service';
import { CompanyService } from 'src/app/app/services/company.service';
import { ProductsService } from 'src/app/app/services/products.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.css']
})
export class PolicyComponent implements OnInit, OnDestroy {
  PaymentMethods = [
    {id: 0, name: 'Cash'},
    {id: 1, name: 'Mobile Money'},
    {id: 2, name: 'Credit Card'},
    {id: 3, name: 'Standing Order'},
    {id: 4, name: 'Payroll'},
    {id: 2, name: 'Other'}
  ];

  PaymentFrequencies = [
    {id: 0, name: 'Monthly'},
    {id: 1, name: 'Every two Months'},
    {id: 2, name: 'Quartely'},
    {id: 5, name: 'Half yearly'},
    {id: 4, name: 'Annual'}
  ];

  PolicyStatuses = [
    {id: 1, name: 'Active'},
    {id: 0, name: 'Prospect'},
    {id: 3, name: 'Canceled'},
    {id: 2, name: 'History'},
  ];

  saving = false;

  currentUserValue: User;
  private subscriptions: Subscription = new Subscription();

  // Filtering
  clients: IClient[];
  filteredClients: Observable<IClient[]>;

  constructor(public policiesService: PoliciesService,
              public clientsService: ClientsService,
              public companyService: CompanyService,
              public productService: ProductsService,
              public agentService: AgentsService,
              private notificationService: NotificationService,
              private authenticationService: AuthenticationService,
              public dialogRef: MatDialogRef<PolicyComponent>) { }


  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.authenticationService.currentUser.subscribe(
        (res: User) => {
          this.currentUserValue = res;
        }
    ));
    if (!this.clientsService.wasFetched) {
      this.clientsService.onGet(this.currentUserValue.workSpaces[0].id);
    }
    this.subscriptions.add(
      this.clientsService.dataSource.subscribe(
        res => {
          this.clients = res;
        }
      )
    );

    if (!this.companyService.wasFetched) {
      this.companyService.onGet(this.currentUserValue.workSpaces[0].id);
    }

    if (!this.productService.wasFetched) {
      this.productService.onGet(this.currentUserValue.workSpaces[0].id);
    }
    if (!this.agentService.wasFetched) {
      this.agentService.onGet(this.currentUserValue.workSpaces[0].id);
    }

    // populate filtered lists
    this.filteredClients = this.policiesService.form.controls.clientId.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : this.displayClientName(value)),
        map(clientName => clientName ? this.filterClients(clientName) : this.clients.slice())
      );
  }

  private filterClients(name: string): IClient[] {
    const filterValue = name.toLowerCase();
    return this.clients.filter(option => option.clientName.toLowerCase().indexOf(filterValue) === 0);
  }

  onProductChange($event) {
    console.log($event);
  }

  onSubmit() {
    if (this.policiesService.form.valid) {
      this.saving = true;
      if (this.policiesService.form.get('id').value !== 0) {
        this.subscriptions.add(
          this.policiesService.onEdit(this.policiesService.form.value)
          .subscribe(
            res => {
              this.notificationService.success(' Updated successfully');
              this.saving = false;
              this.onClose();
            },
            err => {
              this.notificationService.warn(err.error);
              this.saving = false;
            }
        ));
      } else {
        this.subscriptions.add(
          this.policiesService.onCreate(
            this.currentUserValue.workSpaces[0].id,
            this.policiesService.form.value)
            .subscribe(
              res => {
                this.notificationService.success(' Saved successfully');
                this.saving = false;
                this.onClose();
              },
              err => {
                console.log(err);
                this.notificationService.warn(err.error);
                this.saving = false;
              }
        ));
      }
    }
  }

  onClose() {
    this.policiesService.form.reset();
    this.policiesService.initializeFormGroup();
    this.dialogRef.close();
  }

  onClear() {
    this.policiesService.form.reset();
    this.policiesService.initializeFormGroup();
  }

  displayClientName(client: IClient): string {
    return client && client.clientName ? client.clientName : '';
  }

}
