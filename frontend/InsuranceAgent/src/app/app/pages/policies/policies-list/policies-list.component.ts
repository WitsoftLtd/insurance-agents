import { PaymentFrequency, PaymentMethod } from './../../../../shared/enums/enums';
import { PolicyComponent } from './../policy/policy.component';
import { IPolicy, IPolicyModel } from './../../../models/policy';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { User } from 'src/app/shared/models/User';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { PoliciesService } from 'src/app/app/services/policies.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { PolicyStatus } from 'src/app/shared/enums/enums';
import { ClientsService } from 'src/app/app/services/clients.service';

@Component({
  selector: 'app-policies-list',
  templateUrl: './policies-list.component.html',
  styleUrls: ['./policies-list.component.css']
})
export class PoliciesListComponent implements OnInit {

  listData: MatTableDataSource<IPolicy>;
  displayedColumns: string[] = ['policyNumber', 'clientId',  'productId', 'insuranceCompanyId', 'expiryDate', 'premium', 'actions'];
  displayedColumnsMobile: string[] = ['clientId', 'productId', 'premium', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  currentUserValue: User;
  private isHandset: Observable<boolean> = this.breakpointObserver.observe([
    Breakpoints.XSmall
  ])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  isMobile: boolean;

  constructor(
    private policiesService: PoliciesService,
    private clientsService: ClientsService,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private dialog: MatDialog,
    private confrimDialog: DialogService,
    private breakpointObserver: BreakpointObserver) {
    this.authenticationService.currentUser.subscribe(
      (res: User) => {
        this.currentUserValue = res;
      }
    );
    this.isHandset.subscribe(
      res => {
        this.isMobile = res;
      }
    );
  }

  ngOnInit(): void {
    if (!this.clientsService.wasFetched) {
      this.clientsService.onGet(this.currentUserValue.workSpaces[0].id);
    }
    this.policiesService.onGet(this.currentUserValue.workSpaces[0].id);
    this.policiesService.dataSource.subscribe(
      res => {
        this.listData = new MatTableDataSource(res);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele !== 'actions' && data[ele] !== null && data[ele].toLowerCase().indexOf(filter) !== -1;
          });
        };
      }
    );
  }

  getDisplayedColumns(): string[] {
    if (this.isMobile) {
      return this.displayedColumnsMobile;
    } else {
      return this.displayedColumns;
    }
  }

  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter() {
    if (this.searchKey !== null) {
      this.listData.filter = this.searchKey.trim().toLowerCase();
    }
  }

  onCreate() {
    this.policiesService.initializeFormGroup();
    this.dialog.open(PolicyComponent, {
      disableClose: true,
      autoFocus: true,
      width: '100%',
      position: {top: '10px'},
      panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
    });
  }

  onEdit(row: IPolicy){
    this.policiesService.populateForm(row);
    this.dialog.open(PolicyComponent, {
      disableClose: true,
      autoFocus: true,
      width: '100%',
      position: {top: '10px'},
      panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
    });
  }

  onDelete(rowId: number) {
    this.confrimDialog.openConfirmDialog('Are you sure to delete this record ?')
    .afterClosed().subscribe(res => {
      if (res){
        this.policiesService.onDelete(rowId)
        .subscribe(
          _ => {
            this.notificationService.warn(' Deleted successfully');
          },
          err => {
            this.notificationService.warn(err.error);
          }
        );
      }
    });
  }

  getPolicyStatusDescription(status: PolicyStatus) {
    switch (status) {
      case PolicyStatus.Active:
        return 'Active';
      case PolicyStatus.Canceled:
        return 'Canceled';
      case PolicyStatus.History:
        return 'History';
      case PolicyStatus.PreActive:
          return 'Prospect';
    }
  }

  getPaymentFrequencyDescription(frequency: PaymentFrequency) {
    switch (frequency) {
      case PaymentFrequency.Annual:
        return 'Annual';
      case PaymentFrequency.Bi_Monthly:
        return 'Every two Months';
      case PaymentFrequency.Half_Yearly:
        return 'Half yearly';
      case PaymentFrequency.Monthly:
        return 'Monthly';
      case PaymentFrequency.Quartely:
        return 'Quartely';
    }
  }

  getPaymentMethodDescription(payment: PaymentMethod) {
    switch (payment) {
      case PaymentMethod.Cash:
        return 'Cash';
      case PaymentMethod.Debit_Credit_Card:
        return 'Credit Card';
      case PaymentMethod.MobileMoney:
        return 'Mobile Money';
      case PaymentMethod.Other:
        return 'Other';
      case PaymentMethod.Payroll:
        return 'Payroll';
      case PaymentMethod.StandingOrder:
        return 'Standing Order';
    }
  }

}
