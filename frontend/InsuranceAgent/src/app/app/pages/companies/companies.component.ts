import { ICompany } from './../../models/company';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CompanyService } from '../../services/company.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { User } from 'src/app/shared/models/User';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CompanyComponent } from './company/company.component';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  listData: MatTableDataSource<ICompany>;
  displayedColumns: string[] = ['name',  'contactPerson', 'contactPersonPhone', 'email', 'actions'];
  displayedColumnsMobile: string[] = ['name', 'contactPerson', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  currentUserValue: User;
  private isHandset: Observable<boolean> = this.breakpointObserver.observe([
    Breakpoints.XSmall
  ])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  isMobile: boolean;

  constructor(private companyService: CompanyService,
              private authenticationService: AuthenticationService,
              private notificationService: NotificationService,
              private dialog: MatDialog,
              private confrimDialog: DialogService,
              private breakpointObserver: BreakpointObserver) {
    this.authenticationService.currentUser.subscribe(
      (res: User) => {
        this.currentUserValue = res;
      }
    );
    this.isHandset.subscribe(
      res => {
        this.isMobile = res;
      }
    );
  }

  ngOnInit(): void {
    this.companyService.onGet(this.currentUserValue.workSpaces[0].id);
    this.companyService.dataSource.subscribe(
      res => {
        this.listData = new MatTableDataSource(res);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele !== 'actions' && data[ele] !== null && data[ele].toLowerCase().indexOf(filter) !== -1;
          });
        };
      }
    );
  }

  getDisplayedColumns(): string[] {
    if (this.isMobile) {
      return this.displayedColumnsMobile;
    } else {
      return this.displayedColumns;
    }
  }

  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter() {
    if (this.searchKey !== null) {
      this.listData.filter = this.searchKey.trim().toLowerCase();
    }
  }

  onCreate() {
    this.companyService.initializeFormGroup();
    this.dialog.open(CompanyComponent, {
      disableClose: true,
      autoFocus: true,
      width: '100%',
      position: {top: '10px'},
      panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
    });
  }

  onEdit(row: ICompany){
    this.companyService.populateForm(row);
    this.dialog.open(CompanyComponent, {
      disableClose: true,
      autoFocus: true,
      width: '100%',
      position: {top: '10px'},
      panelClass: this.isMobile ? 'form-dialog-container-mobile' : 'form-dialog-container'
    });
  }

  onDelete(rowId: number) {
    this.confrimDialog.openConfirmDialog('Are you sure to delete this record ?')
    .afterClosed().subscribe(res => {
      if (res){
        this.companyService.onDelete(rowId)
        .subscribe(
          _ => {
            this.notificationService.warn(' Deleted successfully');
          },
          err => {
            this.notificationService.warn(err.error);
          }
        );
      }
    });
  }

}
