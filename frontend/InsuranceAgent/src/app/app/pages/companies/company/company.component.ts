import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CompanyService } from 'src/app/app/services/company.service';
import { User } from 'src/app/shared/models/User';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  currentUserValue: User;

  constructor(public companyService: CompanyService,
              private notificationService: NotificationService,
              private authenticationService: AuthenticationService,
              public dialogRef: MatDialogRef<CompanyComponent>) { }

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe(
      (res: User) => {
        this.currentUserValue = res;
      }
    );
  }

  onSubmit() {
    if (this.companyService.form.valid) {
      if (this.companyService.form.get('id').value !== 0) {
          this.companyService.onEdit(this.companyService.form.value)
          .subscribe(
            res => {
              this.notificationService.success(' Updated successfully');
              this.onClose();
            },
            err => {
              this.notificationService.warn(err.error);
            }
          );
      } else {
          this.companyService.onCreate(
            this.currentUserValue.workSpaces[0].id,
            this.companyService.form.value)
            .subscribe(
              res => {
                this.notificationService.success(' Saved successfully');
                this.onClose();
              },
              err => {
                console.log(err);
                this.notificationService.warn(err.error);
              }
            );
      }
    }
  }

  onClose() {
    this.companyService.form.reset();
    this.companyService.initializeFormGroup();
    this.dialogRef.close();
  }

  onClear() {
    this.companyService.form.reset();
    this.companyService.initializeFormGroup();
  }

}
