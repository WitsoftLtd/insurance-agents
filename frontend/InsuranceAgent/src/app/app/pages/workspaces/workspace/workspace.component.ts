import { WorkspaceService } from './../../../services/workspace.service';
import { Component, OnDestroy } from '@angular/core';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnDestroy {

  notificationTypes = [
    {id: 0, name: 'Sms Only'},
    {id: 1, name: 'Email Only'},
    {id: 2, name: 'Both'},
    {id: 3, name: 'None'}
  ];
  subscription: Subscription = new Subscription();

  constructor(public workspaceService: WorkspaceService,
              private notificationService: NotificationService,
              private dialogRef: MatDialogRef<WorkspaceComponent>) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSubmit() {
    if (this.workspaceService.form.valid) {
      if (this.workspaceService.form.get('id').value !== 0) {
        this.subscription.add(
          this.workspaceService.onEdit(this.workspaceService.form.value)
          .subscribe(
            res => {
              this.notificationService.success(' Updated successfully');
              this.onClose();
            },
            err => {
              this.notificationService.warn(err.error);
            }
          ));
      }
    }
  }

  onClose() {
    this.workspaceService.populateForm();
    this.dialogRef.close();
  }

  onClear() {
    this.workspaceService.populateForm();
  }
}
