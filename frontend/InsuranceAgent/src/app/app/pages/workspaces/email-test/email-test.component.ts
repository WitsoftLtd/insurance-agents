import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { WorkspaceService } from 'src/app/app/services/workspace.service';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-email-test',
  templateUrl: './email-test.component.html',
  styleUrls: ['./email-test.component.css']
})
export class EmailTestComponent implements OnDestroy {

  subscription: Subscription = new Subscription();
  constructor(public workspaceService: WorkspaceService,
              private notificationService: NotificationService) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


  onSubmit() {
    if (this.workspaceService.emailForm.valid) {
      this.subscription.add(
        this.workspaceService.onTestEmail(this.workspaceService.emailForm.value)
        .subscribe(
          res => {
            this.notificationService.success(' Email sent successfully');
          },
          err => {
            this.notificationService.warn(err.error);
          }
        ));
    }
  }

}
