import { Component, OnDestroy } from '@angular/core';
import { WorkspaceService } from 'src/app/app/services/workspace.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sms-test',
  templateUrl: './sms-test.component.html',
  styleUrls: ['./sms-test.component.css']
})
export class SmsTestComponent implements OnDestroy {

  subscription: Subscription = new Subscription();
  constructor(public workspaceService: WorkspaceService,
              private notificationService: NotificationService) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


  onSubmit() {
    if (this.workspaceService.smsForm.valid) {
      this.subscription.add(
        this.workspaceService.onTestSms(this.workspaceService.smsForm.value)
        .subscribe(
          res => {
            this.notificationService.success(' Sms sent successfully');
          },
          err => {
            this.notificationService.warn(err.error);
          }
        ));
    }
  }

}
