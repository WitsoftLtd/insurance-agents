import { AgentsService } from './../../../services/agents.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/shared/models/User';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { MatDialogRef } from '@angular/material/dialog';
import { CompanyComponent } from '../../companies/company/company.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.css']
})
export class AgentComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription = new Subscription();
  currentUserValue: User;


  constructor(public agentsService: AgentsService,
              private notificationService: NotificationService,
              private authenticationService: AuthenticationService,
              private dialogRef: MatDialogRef<CompanyComponent>) { }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.authenticationService.currentUser.subscribe(
        (res: User) => {
          this.currentUserValue = res;
        }
    ));
  }

  onSubmit() {
    if (this.agentsService.form.valid) {
      if (this.agentsService.form.get('id').value !== 0) {
        this.subscriptions.add(
          this.agentsService.onEdit(this.agentsService.form.value)
          .subscribe(
            res => {
              this.notificationService.success(' Updated successfully');
              this.onClose();
            },
            err => {
              this.notificationService.warn(err.error);
            }
          ));
      } else {
        this.subscriptions.add(
          this.agentsService.onCreate(
            this.currentUserValue.workSpaces[0].id,
            this.agentsService.form.value)
            .subscribe(
              res => {
                this.notificationService.success(' Saved successfully');
                this.onClose();
              },
              err => {
                console.log(err);
                this.notificationService.warn(err.error);
              }
            ));
      }
    }
  }

  onClose() {
    this.agentsService.form.reset();
    this.agentsService.initializeFormGroup();
    this.dialogRef.close();
  }

  onClear() {
    this.agentsService.form.reset();
    this.agentsService.initializeFormGroup();
  }

}
