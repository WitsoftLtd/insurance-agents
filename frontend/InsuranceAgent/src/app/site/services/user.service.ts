import { Injectable } from '@angular/core';
import { Constants } from '../helpers/constants';
import { HttpClient } from '@angular/common/http';
import { RegisterModel } from '../models/userModel';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from 'src/app/shared/models/User';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private constants = new Constants();
  public currentUser: Observable<User>;
  private currentUserSubject: BehaviorSubject<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  Register(userModel: RegisterModel){
    return this.http.post<User>(`${this.constants.baseUrl}Accounts/Register`, userModel)
    .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
    }));
  }

  forgotPassword(emailAddress: string) {
    return this.http.post(this.constants.baseUrl + 'Accounts/ForgotPassword', {email: emailAddress});
  }
}
