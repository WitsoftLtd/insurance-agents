import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RegisterModel } from '../models/userModel';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {

  userModel: BehaviorSubject<RegisterModel>;
  constructor() {
    this.userModel = new BehaviorSubject({
      email: '',
      password: '',
      confirmPassword: '',
      fullName: '',
      phoneNumber: '',
      workSpaceType: 0
    });
  }

  setUser(model: RegisterModel) {
    this.userModel.next(model);
  }
}
