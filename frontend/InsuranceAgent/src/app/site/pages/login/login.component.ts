import { Component, OnInit } from '@angular/core';
import { Constants } from '../../helpers/constants';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Title, Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { LoginModel } from '../../models/loginModel';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  readonly constants = new Constants();
  formGroup: FormGroup;

  constructor(
    private titleService: Title,
    private metaService: Meta,
    private fb: FormBuilder,
    private router: Router,
    private userService: AuthenticationService
  ) {
      this.formGroup = fb.group({
        email: new FormControl('',
        [
          Validators.required,
          Validators.email
        ]),
        password: new FormControl('',
        [
          Validators.required
        ])
      });
   }

  ngOnInit(): void {
    this.titleService.setTitle(this.constants.applicationName + ' | Login');
    // tslint:disable-next-line: max-line-length
    this.metaService.updateTag({name: 'description', content: 'More businesses and their teams are trusting ' + this.constants.applicationName});
    if (this.userService.currentUserValue) {
      this.router.navigate(['/app/policies']);
    }
  }

  getEmailErrors() {
    if (this.formGroup.get('email').hasError('required')) {
      return 'Email is required';
    }

    if (this.formGroup.get('email').hasError('email')) {
      return 'Invalid email';
    }
  }

  getPasswordErrors() {
    if (this.formGroup.get('password').hasError('required')) {
      return 'Password is required';
    }
  }

  submitForm(postModel: LoginModel) {
    if (this.formGroup.valid) {
        this.userService.login(postModel)
    .subscribe(
      (res: any) => {
        if (res.succeeded) {
          this.router.navigate(['/app/policies']);
        } else {
          // console.log(res);
        }
      },
      err => {
        // console.log(err);
      }
    );
    }
  }

}
