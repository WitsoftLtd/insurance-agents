import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Constants } from '../../helpers/constants';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  readonly constants = new Constants();
  formGroup: FormGroup;

  constructor(
    private titleService: Title,
    private metaService: Meta,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {
    this.formGroup = fb.group({
      email: new FormControl('',
      [
        Validators.required,
        Validators.email
      ])
    });
   }

  ngOnInit(): void {
    this.titleService.setTitle(this.constants.applicationName + ' | Forgot password');
    // tslint:disable-next-line: max-line-length
    this.metaService.updateTag({name: 'description', content: 'More businesses and their teams are trusting ' + this.constants.applicationName});
  }

  getEmailErrors() {
    if (this.formGroup.get('email').hasError('required')) {
      return 'Email is required';
    }

    if (this.formGroup.get('email').hasError('email')) {
      return 'Invalid email';
    }
  }

  submitForm() {
    if (this.formGroup.valid) {
      this.userService.forgotPassword(this.formGroup.get('email').value)
      .subscribe(
        (res: any) => {
          if (res.succeeded) {
            this.router.navigate(['/login']);
          } else {
            this.router.navigate(['/login']);
          }
        },
        err => {
          console.log(err);
        }
      );
    }
  }

}
