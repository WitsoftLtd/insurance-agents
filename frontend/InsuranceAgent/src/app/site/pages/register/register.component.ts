import { DataSharingService } from './../../services/data-sharing.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfirmedValidator } from '../../helpers/ConfirmedValidator';
import { RegisterModel } from '../../models/userModel';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { Constants } from '../../helpers/constants';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  private constants = new Constants();
  registerForm: FormGroup;
  hasServerError = false;
  serverError: string;

  subscriptionTypes = [
    {id: 0, name: 'Agent (Individual)'},
    {id: 1, name: 'Insurance Agency'}
  ];

  constructor(
    private titleService: Title,
    private metaService: Meta,
    private fb: FormBuilder,
    private dataShare: DataSharingService,
    private userService: UserService,
    private router: Router) {
    this.registerForm = fb.group({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      fullName: new FormControl('', [
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      confirmPassword: new FormControl('', [
        Validators.required
      ]),
      phoneNumber: new FormControl(''),
      workSpaceType: new FormControl(0)
    }, {
      validator: ConfirmedValidator('password', 'confirmPassword')
    });
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.constants.applicationName + ' | Start your free trial');
    this.metaService.updateTag({name: 'description', content: this.constants.applicationName + ' offers fully integrated online policy management and much more.'});
  }

  getEmailErrors() {
    if (this.registerForm.get('email').hasError('required')) {
      return 'Email is required';
    }

    if (this.registerForm.get('email').hasError('email')) {
      return 'Invalid email';
    }
  }

  getFullNameErrors() {
    return this.registerForm.get('fullName').hasError('required') ? 'Name is required' : '';
  }

  getPasswordErrors() {
    if (this.registerForm.get('password').hasError('required')) {
      return 'Password is required';
    }
    if (this.registerForm.get('password').errors.minlength) {
      return 'Password must be at least 6 characters long.';
    }
  }

  getConfirmPasswordErrors(): string {
    if (this.registerForm.get('confirmPassword').hasError('required')) {
      return 'Confrim password required';
    }
    if (this.registerForm.controls.confirmPassword.errors.confirmedValidator) {
      return 'Password and Confirm Password must be match';
    }
  }

  submitForm(postModel: RegisterModel) {
    if (this.registerForm.valid) {
      this.hasServerError = false;
      this.userService.Register(postModel).subscribe(
      (res: any) => {
        if (res.succeeded) {
          postModel.password = '';
          postModel.confirmPassword = '';
          this.dataShare.setUser(postModel);
          this.router.navigate(['/app/policies']);
        } else {
          this.hasServerError = true;
          this.serverError = res.errors[0].description;
        }
      },
      err => {
        console.log(err);
        this.hasServerError = true;
        this.serverError = 'Please try again, unknown error occurred in your request.';
      });
    }
  }

}
