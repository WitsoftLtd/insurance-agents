import { Component, OnInit } from '@angular/core';
import { RegisterModel } from '../../models/userModel';
import { DataSharingService } from '../../services/data-sharing.service';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {

  userModel: RegisterModel;
  constructor(private dataShare: DataSharingService) { }

  ngOnInit(): void {
    this.dataShare.userModel.subscribe(
      res => {
        this.userModel = res;
    });
  }

}
