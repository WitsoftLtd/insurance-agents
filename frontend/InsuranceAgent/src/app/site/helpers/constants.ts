import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Constants {
  baseUrl = 'http://localhost:64742/api/'; // development
  // baseUrl = 'https://agents.witsoft.co.ke/api/'; // production
  applicationName = 'Agents Journal';

  constructor() {
    if (environment.production) {
      this.baseUrl = 'https://agents.witsoft.co.ke/api/';
    } else {
      this.baseUrl = 'http://localhost:64742/api/';
    }
  }
}
