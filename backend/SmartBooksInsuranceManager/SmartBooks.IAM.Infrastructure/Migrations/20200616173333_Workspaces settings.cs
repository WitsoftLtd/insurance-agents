﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartBooks.IAM.Infrastructure.Migrations
{
    public partial class Workspacessettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmailSubject",
                table: "WorkSpaces",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NoofDaysToFirstNotification",
                table: "WorkSpaces",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NotificationFrequency",
                table: "WorkSpaces",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailSubject",
                table: "WorkSpaces");

            migrationBuilder.DropColumn(
                name: "NoofDaysToFirstNotification",
                table: "WorkSpaces");

            migrationBuilder.DropColumn(
                name: "NotificationFrequency",
                table: "WorkSpaces");
        }
    }
}
