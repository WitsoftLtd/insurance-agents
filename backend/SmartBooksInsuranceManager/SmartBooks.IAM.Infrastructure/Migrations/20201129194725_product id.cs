﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartBooks.IAM.Infrastructure.Migrations
{
    public partial class productid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropIndex(
                name: "IX_ProductDetails_CustomFieldId",
                table: "ProductDetails");

            migrationBuilder.DropColumn(
                name: "CustomFieldId",
                table: "ProductDetails");

            migrationBuilder.AddColumn<string>(
                name: "Value",
                table: "ProductDetails",
                maxLength: 150,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Value",
                table: "ProductDetails");

            migrationBuilder.AddColumn<int>(
                name: "CustomFieldId",
                table: "ProductDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ProductDetails_CustomFieldId",
                table: "ProductDetails",
                column: "CustomFieldId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductDetails_CustomFields_CustomFieldId",
                table: "ProductDetails",
                column: "CustomFieldId",
                principalTable: "CustomFields",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
