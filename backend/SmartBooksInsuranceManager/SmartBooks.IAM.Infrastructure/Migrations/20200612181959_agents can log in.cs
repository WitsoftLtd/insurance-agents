﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartBooks.IAM.Infrastructure.Migrations
{
    public partial class agentscanlogin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CanLogIn",
                table: "Agents",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CanLogIn",
                table: "Agents");
        }
    }
}
