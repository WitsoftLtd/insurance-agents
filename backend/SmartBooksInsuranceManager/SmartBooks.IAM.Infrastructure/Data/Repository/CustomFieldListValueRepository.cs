﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class CustomFieldListValueRepository<T> : Repository<T>, ICustomFieldListValue<T>
        where T : CustomFieldListValue
    {
        public CustomFieldListValueRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
