﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class AppointmentsRepository<T> : Repository<T>, IAppointment<T>
        where T : Appointment
    {
        public AppointmentsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
