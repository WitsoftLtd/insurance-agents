﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class ClientsRepository<T> : Repository<T>, IClient<T>
        where T : Client
    {
        public ClientsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
