﻿using Microsoft.EntityFrameworkCore;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class AgentsRepository<T> : Repository<T>, IAgent<T>
        where T : Agent
    {
        public AgentsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }

        public async Task<ICollection<T>> Agents(int workSpaceId)
        {
            return await insuranceContext.Set<T>()
                .Include(e => e.ApplicationUser)
                .Where(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false)
                .OrderBy(e => e.ApplicationUser.FullName)
                .ToListAsync();
        }

        public async Task<T> GetDetailedUser(int id)
        {
            return await insuranceContext.Set<T>()
                 .Include(e => e.ApplicationUser)
                 .Where(e => e.Id == id)
                 .FirstOrDefaultAsync();
        }
    }
}
