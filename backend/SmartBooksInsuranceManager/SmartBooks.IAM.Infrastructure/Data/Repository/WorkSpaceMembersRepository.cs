﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Subscription.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class WorkSpaceMembersRepository<T> : Repository<T>, IWorkSpaceMember<T>
        where T : WorkSpaceMember
    {
        public WorkSpaceMembersRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
