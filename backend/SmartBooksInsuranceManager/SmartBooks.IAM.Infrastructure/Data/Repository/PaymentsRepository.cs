﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class PaymentsRepository<T> : Repository<T>, IPayment<T>
        where T : Payment
    {
        public PaymentsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
