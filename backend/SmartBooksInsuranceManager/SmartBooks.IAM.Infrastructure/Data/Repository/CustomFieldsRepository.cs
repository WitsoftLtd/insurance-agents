﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class CustomFieldsRepository<T> : Repository<T>, ICustomField<T>
        where T : CustomField
    {
        public CustomFieldsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
