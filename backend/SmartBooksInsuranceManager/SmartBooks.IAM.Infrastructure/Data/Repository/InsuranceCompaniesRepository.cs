﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;
using System.Linq;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class InsuranceCompaniesRepository<T> : Repository<T>, IInsuranceCompany<T>
        where T : InsuranceCompany
    {
        public InsuranceCompaniesRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }

        public bool DuplicateName(int id, string name, int workSpaceId)
        {
            return insuranceContext.Set<T>()
               .Any(e => e.Name == name && e.WorkSpaceId == workSpaceId && e.Id != id);
        }

        public bool NameExists(string name, int workSpaceId)
        {
            return insuranceContext.Set<T>()
                .Any(e => e.WorkSpaceId == workSpaceId && e.Name == name);
        }
    }
}
