﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class FilesRepository<T> : Repository<T>, IFile<T>
        where T : File
    {
        public FilesRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
