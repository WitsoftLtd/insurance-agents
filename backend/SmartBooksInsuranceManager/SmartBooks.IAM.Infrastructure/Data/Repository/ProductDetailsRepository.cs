﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class ProductDetailsRepository<T> : Repository<T>, IProductDetail<T>
        where T : ProductDetail
    {
        public ProductDetailsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
