﻿using Microsoft.EntityFrameworkCore;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class PoliciesRepository<T> : Repository<T>, IPolicy<T>
        where T : Policy
    {
        public PoliciesRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }

        public bool DuplicatePolicy(int id, string number, int workSpaceId)
        {
            return insuranceContext.Set<T>()
               .Any(e => e.PolicyNumber == number && e.WorkSpaceId == workSpaceId && e.Id != id);
        }

        public async Task<T> GetDetailed(int id)
        {
            return await insuranceContext.Set<T>()
                .Include(e => e.Client)
                .Include(e => e.InsuranceCompany)
                .Include(e => e.Product)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();
        }

        public bool PolicyExists(string number, int workSpaceId)
        {
            return insuranceContext.Set<T>()
                .Any(e => e.WorkSpaceId == workSpaceId && e.PolicyNumber == number);
        }
    }
}
