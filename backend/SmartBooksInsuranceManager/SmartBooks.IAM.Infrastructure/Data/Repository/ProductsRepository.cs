﻿using Microsoft.EntityFrameworkCore;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;
using System.Collections.Generic;
using System.Linq;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class ProductsRepository<T> : Repository<T>, IProduct<T>
        where T : Product
    {
        public ProductsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }

        public bool DuplicateName(int id, string name, int workSpaceId)
        {
            return insuranceContext.Set<T>()
               .Any(e => e.Name == name && e.WorkSpaceId == workSpaceId && e.Id != id);
        }

        public bool NameExists(string name, int workSpaceId)
        {
            return insuranceContext.Set<T>()
                .Any(e => e.WorkSpaceId == workSpaceId && e.Name == name);
        }

        public ICollection<Product> Products(int workSpaceId)
        {
            ICollection<Product> products =
                insuranceContext.Products
                .Include(e => e.ProductDetails)
                .Where(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false)
                .OrderBy(e => e.Name)
                .ToList();
            foreach (var item in products)
            {
                foreach (var prdItem in item.ProductDetails)
                {
                    prdItem.Product = null;
                }
            }
            return products;
        }
    }
}
