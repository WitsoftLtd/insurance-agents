﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class ClaimsRepositoty<T> : Repository<T>, IClaim<T>
        where T : Claim
    {
        public ClaimsRepositoty(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
