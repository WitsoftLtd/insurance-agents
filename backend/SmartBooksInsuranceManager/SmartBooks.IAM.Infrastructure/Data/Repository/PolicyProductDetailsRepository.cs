﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class PolicyProductDetailsRepository<T> : Repository<T>, IPolicyProductDetail<T>
        where T : PolicyProductDetail
    {
        public PolicyProductDetailsRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
