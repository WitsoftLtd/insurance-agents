﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Subscription.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class WorkSpaceRepository<T> : Repository<T>, IWorkSpace<T>
        where T : WorkSpace
    {
        public WorkSpaceRepository(InsuranceContext _insuranceContext) : base(_insuranceContext)
        {
        }
    }
}
