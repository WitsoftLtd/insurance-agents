﻿using Microsoft.EntityFrameworkCore;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Subscription.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace SmartBooks.IAM.Infrastructure.Data.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly InsuranceContext insuranceContext;
        public Repository(InsuranceContext _insuranceContext)
        {
            insuranceContext = _insuranceContext;
        }

        public void Add(T entity)
        {
            insuranceContext.Set<T>().Add(entity);
        }
        public void AddRange(IReadOnlyList<T> entities)
        {
            insuranceContext.Set<T>().AddRange(entities);
        }
        public async Task<int> CountAsync(ISpecification<T> spec)
        {
            return await ApplySpecification(spec).CountAsync();
        }

        public void Delete(T entity)
        {
            insuranceContext.Set<T>().Remove(entity);
        }

        public void DeleteRange(IReadOnlyList<T> entities)
        {
            insuranceContext.Set<T>().RemoveRange(entities);
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await insuranceContext.Set<T>().FindAsync(id);
        }

        public async Task<IReadOnlyList<T>> ListAllAsync()
        {
            return await insuranceContext.Set<T>().ToListAsync();
        }

        public async Task<ICollection<T>> ListAsync(ISpecification<T> spec)
        {
            return await ApplySpecification(spec).ToListAsync();
        }

        public void Update(T entity)
        {
            insuranceContext.Entry<T>(insuranceContext.Set<T>().Find(entity.Id)).CurrentValues.SetValues(entity);
        }

        private IQueryable<T> ApplySpecification(ISpecification<T> spec)
        {
            return SpecificationEvaluator<T>.GetQuery(insuranceContext.Set<T>().AsQueryable(), spec);
        }

        public int SaveChanges()
        {
            return insuranceContext.SaveChanges();
        }

        public int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            return insuranceContext.SaveChanges(acceptAllChangesOnSuccess);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await insuranceContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            return await insuranceContext.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return insuranceContext.Set<T>()
                .Where(predicate);
        }
    }
}
