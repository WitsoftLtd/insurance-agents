﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using SmartBooks.IAM.Entities.Entities;
using SmartBooks.IAM.Subscription.Entities;
using System;
using System.Reflection;
using UtilitiesLicensing.Models;

namespace SmartBooks.IAM.Infrastructure.Data
{
    public class InsuranceContextFactory : IDesignTimeDbContextFactory<InsuranceContext>
    {
        public InsuranceContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<InsuranceContext>();
            optionsBuilder.UseMySQL("server=localhost;database=InsuranceDB;user=root;password=RMEK6078");
            return new InsuranceContext(optionsBuilder.Options);
        }
    }
    public class InsuranceContext : DbContext
    {
        public InsuranceContext(DbContextOptions<InsuranceContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            if (builder == null) throw new NotImplementedException();

            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            builder.Entity<IdentityUserRole<int>>().HasKey(e => e.RoleId);
        }

        //Subscription and Security
        //Security
        public DbSet<Role> Roles { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<IdentityUserRole<int>> IdentityUserRoles { get; set; }
        public DbSet<RoleClaim> RoleClaims { get; set; }
        public DbSet<IdentityUserClaim<int>> IdentityUserClaims { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }

        //Subscription
        public DbSet<WorkSpace> WorkSpaces { get; set; }
        public DbSet<WorkSpaceMember> WorkSpaceMembers { get; set; }

        //Application
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<CustomField> CustomFields { get; set; }
        public DbSet<CustomFieldListValue> CustomFieldListValues { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<InsuranceCompany> InsuranceCompanies { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Policy> Policies { get; set; }
        public DbSet<PolicyProductDetail> PolicyProductDetails { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<MpesaClient> MpesaClients { get; set; }
        public DbSet<LicenseLog> LicenseLogs { get; set; }

    }
}
