﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartBooks.IAM.Subscription.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Config
{
    public class WorkSpacesConfiguration : IEntityTypeConfiguration<WorkSpaceMember>
    {
        public void Configure(EntityTypeBuilder<WorkSpaceMember> builder)
        {
            builder.HasOne(e => e.Member)
                .WithMany(e => e.WorkSpaceMembers)
                .HasForeignKey(e => e.MemberId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.InvitedBy)
                .WithMany()
                .HasForeignKey(e => e.InvitedById)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(e => e.WorkSpace)
                .WithMany(e => e.WorkSpaceMembers)
                .HasForeignKey(e => e.WorkSpaceId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
