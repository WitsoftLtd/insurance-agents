﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Config
{
    public class CustomFieldsConfiguration : IEntityTypeConfiguration<CustomFieldListValue>
    {
        public void Configure(EntityTypeBuilder<CustomFieldListValue> builder)
        {
            builder.HasOne(e => e.CustomField)
                .WithMany(e => e.CustomFieldListValues)
                .HasForeignKey(e => e.CustomFieldId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
