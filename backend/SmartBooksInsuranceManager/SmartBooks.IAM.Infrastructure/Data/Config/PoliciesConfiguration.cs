﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.Infrastructure.Data.Config
{
    public class PoliciesConfiguration : IEntityTypeConfiguration<Policy>
    {
        public void Configure(EntityTypeBuilder<Policy> builder)
        {
            builder.HasOne(e => e.Client)
                .WithMany()
                .HasForeignKey(e => e.ClientId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.Agent)
                .WithMany()
                .HasForeignKey(e => e.AgentId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.Product)
                .WithMany()
                .HasForeignKey(e => e.ProductId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.InsuranceCompany)
                .WithMany()
                .HasForeignKey(e => e.InsuranceCompanyId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class PolicyProductDetailsConfiguration : IEntityTypeConfiguration<PolicyProductDetail>
    {
        public void Configure(EntityTypeBuilder<PolicyProductDetail> builder)
        {
            builder.HasOne(e => e.Policy)
                .WithMany(e => e.PolicyProductDetails)
                .HasForeignKey(e => e.ProductDetailId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.ProductDetail)
                .WithMany()
                .HasForeignKey(e => e.ProductDetailId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
