﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Specifications;
using SmartBooks.IAM.AgentsManager.ViewModels;
using SmartBooks.IAM.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Services
{
    public class PoliciesService
    {
        private readonly IPolicy<Policy> policy;
        public PoliciesService(IPolicy<Policy> _policy)
        {
            policy = _policy;
        }

        public async Task<ICollection<Policy>> GetAllAsync(int workSpaceId)
        {
            PoliciesSpecifications specifications = new PoliciesSpecifications(workSpaceId);
            return await policy.ListAsync(specifications);
        }

        public async Task<Policy> AddAsync(PolicyViewModel viewModel, int workSpaceId, DateTimeOffset dateTimeOffset)
        {
            if (string.IsNullOrWhiteSpace(viewModel.PolicyNumber))
            {
                throw new Exception("Policy number requires a value");
            }
            if (policy.PolicyExists(viewModel.PolicyNumber, workSpaceId))
            {
                throw new Exception("A record with a similar policy number already exist");
            }

            Policy newPolicy = new Policy
            {
                AdvancePayment = viewModel.AdvancePayment,
                AgentId = viewModel.AgentId,
                ClientId = viewModel.ClientId,
                Deductible = viewModel.Deductible,
                EffectiveDate = viewModel.EffectiveDate,
                ExpiryDate = viewModel.ExpiryDate,
                InsuranceCompanyId = viewModel.InsuranceCompanyId,
                InsuredSummary = viewModel.InsuredSummary,
                PaymentFrequency = viewModel.PaymentFrequency,
                PaymentMethod = viewModel.PaymentMethod,
                PolicyNumber = viewModel.PolicyNumber,
                PolicyStatus = viewModel.PolicyStatus,
                Premium = viewModel.Premium,
                ProductId = viewModel.ProductId,
                SalesCommission = viewModel.SalesCommission,
                TermLength = viewModel.TermLength,
                TotalCharged = viewModel.TotalCharged,
                TimeCreated = dateTimeOffset,
                WorkSpaceId = workSpaceId,
            };

            policy.Add(newPolicy);
            await policy.SaveChangesAsync();
            return await policy.GetDetailed(newPolicy.Id);
        }

        public async Task<Policy> UpdateAsync(PolicyViewModel viewModel)
        {
            if (string.IsNullOrWhiteSpace(viewModel.PolicyNumber))
            {
                throw new Exception("Policy number requires a value");
            }
            Policy pol = await policy.GetByIdAsync(viewModel.Id);
            if (pol == null)
            {
                throw new Exception("Record not found");
            }
            if (policy.DuplicatePolicy(viewModel.Id, viewModel.PolicyNumber, pol.WorkSpaceId))
            {
                throw new Exception("Updating policy with the provided policy number would create a duplicate record");
            }

            pol.AdvancePayment = viewModel.AdvancePayment;
            pol.AgentId = viewModel.AgentId;
            pol.ClientId = viewModel.ClientId;
            pol.Deductible = viewModel.Deductible;
            pol.EffectiveDate = viewModel.EffectiveDate;
            pol.ExpiryDate = viewModel.ExpiryDate;
            pol.InsuranceCompanyId = viewModel.InsuranceCompanyId;
            pol.InsuredSummary = viewModel.InsuredSummary;
            pol.PaymentFrequency = viewModel.PaymentFrequency;
            pol.PaymentMethod = viewModel.PaymentMethod;
            pol.PolicyNumber = viewModel.PolicyNumber;
            pol.PolicyStatus = viewModel.PolicyStatus;
            pol.Premium = viewModel.Premium;
            pol.ProductId = viewModel.ProductId;
            pol.SalesCommission = viewModel.SalesCommission;
            pol.TermLength = viewModel.TermLength;
            pol.TotalCharged = viewModel.TotalCharged;

            policy.Update(pol);
            await policy.SaveChangesAsync();
            return await policy.GetDetailed(pol.Id);
        }

        public async Task<int> DeleteAsync(int id)
        {
            policy.Delete(await policy.GetByIdAsync(id));
            await policy.SaveChangesAsync();
            return id;
        }
    }
}
