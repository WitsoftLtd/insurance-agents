﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Specifications;
using SmartBooks.IAM.Subscription.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Services
{
    public class WorkSpaceMemberService
    {
        private readonly IWorkSpaceMember<WorkSpaceMember> workSpaceMember;

        public WorkSpaceMemberService(IWorkSpaceMember<WorkSpaceMember> _workSpaceMember)
        {
            workSpaceMember = _workSpaceMember;
        }

        public async Task<bool> AddAsync(WorkSpaceMember spaceMember)
        {
            workSpaceMember.Add(spaceMember);
            return await workSpaceMember.SaveChangesAsync() > 0;
        }

        public async Task<ICollection<WorkSpaceMember>> WorkSpaceMembersByUser(int userId)
        {
            WorkSpaceMembersSpecification membersSpecification = new WorkSpaceMembersSpecification(userId);
            return await workSpaceMember.ListAsync(membersSpecification);
        }

    }
}
