﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.Subscription.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Services
{
    public class WorkSpaceService
    {
        private readonly IWorkSpace<WorkSpace> workSpace;
        public WorkSpaceService(IWorkSpace<WorkSpace> _workSpace)
        {
            workSpace = _workSpace;
        }

        public async Task<WorkSpace> AddAsync(WorkSpace workSpaceModel)
        {
            workSpaceModel.Description = string.IsNullOrWhiteSpace(workSpaceModel.Description) ? 
                workSpaceModel.Name : workSpaceModel.Description;
            workSpace.Add(workSpaceModel);
            await workSpace.SaveChangesAsync();
            return workSpaceModel;
        }
    }
}
