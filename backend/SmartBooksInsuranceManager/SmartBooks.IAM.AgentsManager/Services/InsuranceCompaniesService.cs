﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Specifications;
using SmartBooks.IAM.AgentsManager.ViewModels;
using SmartBooks.IAM.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Services
{
    public class InsuranceCompaniesService
    {
        private readonly IInsuranceCompany<InsuranceCompany> insuranceCompany;
        public InsuranceCompaniesService(IInsuranceCompany<InsuranceCompany> _insuranceCompany)
        {
            insuranceCompany = _insuranceCompany;
        }
        public async Task<ICollection<InsuranceCompany>> GetInsuranceCompaniesAsync(int workSpaceId)
        {
            InsuranceCompaniesSpecifications companiesSpecifications = new InsuranceCompaniesSpecifications(workSpaceId);
            return await insuranceCompany.ListAsync(companiesSpecifications);
        }

        public async Task<InsuranceCompany> AddAsync(CompanyViewModel viewModel, int workSpaceId, DateTimeOffset dateTimeOffset)
        {
            if (string.IsNullOrWhiteSpace(viewModel.Name))
            {
                throw new Exception("Name requires a value");
            }
            if (insuranceCompany.NameExists(viewModel.Name, workSpaceId))
            {
                throw new Exception("A company with a similar name already exist");
            }
            InsuranceCompany company = new InsuranceCompany
            {
                Address = viewModel.Address,
                ContactPerson = viewModel.ContactPerson,
                ContactPersonPhone = viewModel.ContactPersonPhone,
                Details = viewModel.Details,
                Email = viewModel.Email,
                IsDeleted = false,
                Name = viewModel.Name,
                TimeCreated = dateTimeOffset,
                WebAddress = viewModel.WebAddress,
                WorkSpaceId = workSpaceId
            };
            insuranceCompany.Add(company);
            await insuranceCompany.SaveChangesAsync();
            return company;
        }

        public async Task<InsuranceCompany> UpdateAsync(CompanyViewModel viewModel)
        {
            if (string.IsNullOrWhiteSpace(viewModel.Name))
            {
                throw new Exception("Name requires a value");
            }
            InsuranceCompany company = await insuranceCompany.GetByIdAsync(viewModel.Id);
            if (company == null)
            {
                throw new Exception("Record not found");
            }
            if (insuranceCompany.DuplicateName(viewModel.Id, viewModel.Name, company.WorkSpaceId))
            {
                throw new Exception("Updating company with the provided code would create a duplicate record");
            }
            company.Address = viewModel.Address;
            company.ContactPerson = viewModel.ContactPerson;
            company.ContactPersonPhone = viewModel.ContactPersonPhone;
            company.Details = viewModel.Details;
            company.Email = viewModel.Email;
            company.IsDeleted = false;
            company.Name = viewModel.Name;
            company.WebAddress = viewModel.WebAddress;
            insuranceCompany.Update(company);
            await insuranceCompany.SaveChangesAsync();
            return company;
        }

        public async Task<int> DeleteAsync(int id)
        {
            insuranceCompany.Delete(await insuranceCompany.GetByIdAsync(id));
            await insuranceCompany.SaveChangesAsync();
            return id;
        }
    }
}
