﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Specifications;
using SmartBooks.IAM.AgentsManager.ViewModels;
using SmartBooks.IAM.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Services
{
    public class ProductsService
    {
        private readonly IProduct<Product> product;
        public ProductsService(IProduct<Product> _product)
        {
            product = _product;
        }

        public async Task<ICollection<Product>> GetAllAsync(int workSpaceId)
        {
            ProductsSpecifications specifications = new ProductsSpecifications(workSpaceId);
            return await product.ListAsync(specifications);
        }

        public ICollection<Product> GetProducts(int workSpaceId)
        {
            return product.Products(workSpaceId);
        }

        public async Task<Product> AddAsync(ProductViewModel viewModel, int workSpaceId, DateTimeOffset dateTimeOffset)
        {
            if (string.IsNullOrWhiteSpace(viewModel.Name))
            {
                throw new Exception("Name requires a value");
            }
            if (product.NameExists(viewModel.Name, workSpaceId))
            {
                throw new Exception("A record with a similar name already exist");
            }

            Product newProduct = new Product
            {
                Name = viewModel.Name,
                TimeCreated = dateTimeOffset,
                WorkSpaceId = workSpaceId,
            };

            foreach (var customFieldView in viewModel.CustomFields)
            {
                CustomField customField = new CustomField
                {
                    CustomFieldType = CustomFieldType.Product,
                    DataType = CustomFieldDataType.String,
                    DefaultValue = customFieldView.DefaultValue,
                    Label = customFieldView.Label,
                    Required = customFieldView.Required,
                    TimeCreated = dateTimeOffset,
                    WorkSpaceId = workSpaceId
                };

                newProduct.ProductDetails.Add(
                    new ProductDetail
                    {
                        TimeCreated = dateTimeOffset,
                        WorkSpaceId = workSpaceId
                    });
            }



            product.Add(newProduct);
            await product.SaveChangesAsync();
            return newProduct;
        }

        public async Task<Product> UpdateAsync(ProductViewModel viewModel)
        {
            if (string.IsNullOrWhiteSpace(viewModel.Name))
            {
                throw new Exception("Name requires a value");
            }
            Product prod = await product.GetByIdAsync(viewModel.Id);
            if (prod == null)
            {
                throw new Exception("Record not found");
            }
            if (product.DuplicateName(viewModel.Id, viewModel.Name, prod.WorkSpaceId))
            {
                throw new Exception("Updating company with the provided code would create a duplicate record");
            }

            prod.Name = viewModel.Name;

            product.Update(prod);
            await product.SaveChangesAsync();
            return prod;
        }

        public async Task<int> DeleteAsync(int id)
        {
            product.Delete(await product.GetByIdAsync(id));
            await product.SaveChangesAsync();
            return id;
        }
    }
}
