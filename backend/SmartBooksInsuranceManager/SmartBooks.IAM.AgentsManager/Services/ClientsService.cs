﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Specifications;
using SmartBooks.IAM.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Services
{
    public class ClientsService
    {
        private readonly IClient<Client> client;

        public ClientsService(IClient<Client> _client)
        {
            client = _client;
        }

        public async Task<ICollection<Client>> GetAllAsync(int workSpaceId)
        {
            ClientsSpecifications specifications = new ClientsSpecifications(workSpaceId);
            return await client.ListAsync(specifications);
        }

        public async Task<Client> AddAsync(Client viewModel, int workSpaceId, DateTimeOffset dateTimeOffset)
        {
            if (string.IsNullOrWhiteSpace(viewModel.ClientName))
            {
                throw new Exception("Name requires a value");
            }

            viewModel.WorkSpaceId = workSpaceId;
            viewModel.TimeCreated = dateTimeOffset;

            client.Add(viewModel);
            await client.SaveChangesAsync();
            return viewModel;
        }

        public async Task<Client> UpdateAsync(Client viewModel)
        {
            if (string.IsNullOrWhiteSpace(viewModel.ClientName))
            {
                throw new Exception("Name requires a value");
            }
            Client clnt = await client.GetByIdAsync(viewModel.Id);
            if (clnt == null)
            {
                throw new Exception("Record not found");
            }
            clnt.Address = viewModel.Address;
            clnt.AltPhoneNumber = viewModel.AltPhoneNumber;
            clnt.ClientName = viewModel.ClientName;
            clnt.ClientNumber = viewModel.ClientNumber;
            clnt.ClientStatus = viewModel.ClientStatus;
            clnt.ClientType = viewModel.ClientType;
            clnt.ContactPersonName = viewModel.ContactPersonName;
            clnt.ContactPersonPhone = viewModel.ContactPersonPhone;
            clnt.DateofBirth = viewModel.DateofBirth;
            clnt.Details = viewModel.Details;
            clnt.Email = viewModel.Email;
            clnt.EmergencyContactName = viewModel.EmergencyContactName;
            clnt.EmergencyContactPhone = viewModel.EmergencyContactPhone;
            clnt.Gender = viewModel.Gender;
            clnt.MaritalStatus = viewModel.MaritalStatus;
            clnt.NationalId = viewModel.NationalId;
            clnt.PhoneNumber = viewModel.PhoneNumber;
            clnt.PIN = viewModel.PIN;
            client.Update(clnt);
            await client.SaveChangesAsync();
            return viewModel;
        }

        public async Task<int> DeleteAsync(int id)
        {
            client.Delete(await client.GetByIdAsync(id));
            await client.SaveChangesAsync();
            return id;
        }
    }
}
