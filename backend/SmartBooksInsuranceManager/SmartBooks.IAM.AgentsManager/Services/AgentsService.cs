﻿using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.ViewModels;
using SmartBooks.IAM.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Services
{
    public class AgentsService
    {
        private readonly IAgent<Agent> agent;
        public AgentsService(IAgent<Agent> _agent)
        {
            agent = _agent;
        }

        public async Task<ICollection<AgentViewModel>> GetAgentsAsync(int workSpaceId)
        {
            ICollection<Agent> agents = await agent.Agents(workSpaceId);
            List<AgentViewModel> agentViewModels = new List<AgentViewModel>();
            foreach (var aget in agents)
            {
                agentViewModels.Add(new AgentViewModel
                {
                    ApplicationUserId = aget.ApplicationUserId,
                    CanLogIn = aget.CanLogIn,
                    Email = aget.ApplicationUser.Email,
                    FullName = aget.ApplicationUser.FullName,
                    Id = aget.Id,
                    PhoneNumber = aget.ApplicationUser.PhoneNumber
                });
            }
            return agentViewModels;
        }

        public async Task<int> AddAsync(
            int userId,
            DateTimeOffset dateTimeOffset,
            int workSpaceId,
            bool canLogin = true)
        {
            var newAgent = new Agent
            {
                ApplicationUserId = userId,
                TimeCreated = dateTimeOffset,
                WorkSpaceId = workSpaceId,
                CanLogIn = canLogin
            };
            agent.Add(newAgent);
            await agent.SaveChangesAsync();
            return newAgent.Id;
        }

        public async Task<Agent> GetAgentAsync(int id)
        {
            return await agent.GetByIdAsync(id);
        }

        public async Task<Agent> GetDetailedUserAsync(int id)
        {
            return await agent.GetDetailedUser(id);
        }

        public async Task<bool> UpdateAsync(int id, bool canLogin)
        {
            Agent agnt = await agent.GetByIdAsync(id);
            if (agnt == null)
            {
                throw new Exception("Record not found");
            }
            agnt.CanLogIn = canLogin;
            agent.Update(agnt);
            return await agent.SaveChangesAsync() > 0;
        }

        public async Task<int> DeleteAsync(int id)
        {
            agent.Delete(await agent.GetByIdAsync(id));
            await agent.SaveChangesAsync();
            return id;
        }
    }
}
