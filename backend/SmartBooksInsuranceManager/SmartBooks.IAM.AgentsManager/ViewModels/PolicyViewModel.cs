﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartBooks.IAM.AgentsManager.ViewModels
{
    public class PolicyViewModel
    {
        //public PolicyViewModel()
        //{
        //    PolicyProductDetails = new List<PolicyProductDetail>(); 
        //}
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int InsuranceCompanyId { get; set; }
        public int ProductId { get; set; }
        /// <summary>
        /// Hide for individual workspaces
        /// And pick the user as agent
        /// </summary>
        public int AgentId { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        /// <summary>
        /// In months
        /// ReadOnly field, difference between effective date and expirydate
        /// </summary>
        public int TermLength { get; set; }
        public decimal InsuredSummary { get; set; }
        public decimal Premium { get; set; }
        /// <summary>
        /// Amount policy holder as to pay before insurance company can pay anything
        /// Even if something were to happen before total premium paid is equal or greater
        /// than deductible, you don't get anything
        /// </summary>
        public decimal Deductible { get; set; }
        public decimal SalesCommission { get; set; }
        public PolicyStatus PolicyStatus { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

        //Readonly fields
        public decimal TotalCharged { get; set; }
        public decimal AdvancePayment { get; set; }
        // public List<PolicyProductDetail> PolicyProductDetails { get; set; }

    }
}
