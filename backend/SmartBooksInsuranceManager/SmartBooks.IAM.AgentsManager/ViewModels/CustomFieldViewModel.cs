﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartBooks.IAM.AgentsManager.ViewModels
{
    public class CustomFieldViewModel
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public bool Required { get; set; }
        public string DefaultValue { get; set; }
    }
}
