﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartBooks.IAM.AgentsManager.ViewModels
{
    public class AgentViewModel
    {
        public int Id { get; set; }
        public int ApplicationUserId { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public bool CanLogIn { get; set; }
    }
}
