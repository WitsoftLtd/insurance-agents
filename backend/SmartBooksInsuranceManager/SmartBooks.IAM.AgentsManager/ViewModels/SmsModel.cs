﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartBooks.IAM.AgentsManager.ViewModels
{
    public class SmsModel
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }

    public class EmailModel
    {
        public string Email { get; set; }
        public string EmailSubject { get; set; }
        public string EmailMessage { get; set; }
    }
}
