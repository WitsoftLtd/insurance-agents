﻿using System.Collections.Generic;

namespace SmartBooks.IAM.AgentsManager.ViewModels
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {
            CustomFields = new List<CustomFieldViewModel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CustomFieldViewModel> CustomFields { get; set; }
    }
}
