﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class ClaimsSpecifications : BaseSpecification<Claim>
    {
        public ClaimsSpecifications(int workSpaceId)
            : base(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false) { }

        public ClaimsSpecifications(int policyId, int workSpaceId = 0)
            :base(e => e.IsDeleted == false && e.PolicyId == policyId
            && e.WorkSpaceId == workSpaceId) { }

        protected ClaimsSpecifications(Expression<Func<Claim, bool>> criteria)
          : base(criteria) { }
    }
}
