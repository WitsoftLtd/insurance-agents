﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class InsuranceCompaniesSpecifications : BaseSpecification<InsuranceCompany>
    {
        public InsuranceCompaniesSpecifications(Expression<Func<InsuranceCompany, bool>> criteria) 
            : base(criteria)
        {
        }

        public InsuranceCompaniesSpecifications(int workSpaceId)
            : base(e => e.IsDeleted == false && e.WorkSpaceId == workSpaceId)
        {
        }
    }
}
