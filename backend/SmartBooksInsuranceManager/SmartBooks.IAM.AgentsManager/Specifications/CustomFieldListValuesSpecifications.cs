﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class CustomFieldListValuesSpecifications : BaseSpecification<CustomFieldListValue>
    {
        protected CustomFieldListValuesSpecifications(Expression<Func<CustomFieldListValue, bool>> criteria) 
            : base(criteria)
        {
            AddInclude(e => e.CustomFieldId);
        }

        protected CustomFieldListValuesSpecifications(int customFieldId)
            : base(e => e.IsDeleted == false && e.CustomFieldId == customFieldId)
        {
            AddInclude(e => e.CustomFieldId);
        }
    }
}
