﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class PaymentsSpecifications : BaseSpecification<Payment>
    {
        protected PaymentsSpecifications(Expression<Func<Payment, bool>> criteria) 
            : base(criteria)
        {
            AddInclude(e => e.Policy);
            ApplyOrderByDescending(e => e.TransactionDate);
        }

        protected PaymentsSpecifications(int workSpaceId)
            : base(e => e.IsDeleted == false && e.WorkSpaceId == workSpaceId)
        {
            AddInclude(e => e.Policy);
            ApplyOrderByDescending(e => e.TransactionDate);
        }

        protected PaymentsSpecifications(int policyId, int workSpaceId)
            : base(e => e.IsDeleted == false && e.WorkSpaceId == workSpaceId
            && e.PolicyId == policyId)
        {
            AddInclude(e => e.Policy);
            ApplyOrderByDescending(e => e.TransactionDate);
        }
    }
}
