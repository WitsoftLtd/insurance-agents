﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class PoliciesSpecifications : BaseSpecification<Policy>
    {
        protected PoliciesSpecifications(Expression<Func<Policy, bool>> criteria) 
            : base(criteria)
        {
        }

        public PoliciesSpecifications(int workSpaceId)
            : base(e => e.IsDeleted == false && e.WorkSpaceId == workSpaceId)
        {
            AddInclude(e => e.Client);
            AddInclude(e => e.Agent);
            AddInclude(e => e.InsuranceCompany);
            AddInclude(e => e.Product);
            ApplyOrderByDescending(e => e.ExpiryDate);
        }
    }
}
