﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class PolicyProductDetailsSpecifications : BaseSpecification<PolicyProductDetail>
    {
        protected PolicyProductDetailsSpecifications(Expression<Func<PolicyProductDetail, bool>> criteria) 
            : base(criteria)
        {
            AddInclude(e => e.Policy);
            AddInclude(e => e.ProductDetail);
        }

        protected PolicyProductDetailsSpecifications(int policyId)
            : base(e => e.PolicyId == policyId && e.IsDeleted == false)
        {
            AddInclude(e => e.Policy);
            AddInclude(e => e.ProductDetail);
        }
    }
}
