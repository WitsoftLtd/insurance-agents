﻿using SmartBooks.IAM.Subscription.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class WorkSpaceMembersSpecification : BaseSpecification<WorkSpaceMember>
    {
        public WorkSpaceMembersSpecification(int userId) 
            : base(e => e.MemberId == userId && e.IsDeleted == false)
        {
            AddInclude(e => e.WorkSpace);
        }

        protected WorkSpaceMembersSpecification(Expression<Func<WorkSpaceMember, bool>> criteria)
           : base(criteria) { }
    }
}
