﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class AgentsSpecification : BaseSpecification<Agent>
    {
        public AgentsSpecification(int workSpaceId)
            : base(e => e.IsDeleted == false && e.WorkSpaceId == workSpaceId)
        {
            AddInclude(e => e.ApplicationUser);
        }

        protected AgentsSpecification(Expression<Func<Agent, bool>> criteria)
           : base(criteria) { }
    }
}
