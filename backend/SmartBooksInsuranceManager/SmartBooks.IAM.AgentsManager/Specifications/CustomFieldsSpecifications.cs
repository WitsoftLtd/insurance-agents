﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class CustomFieldsSpecifications : BaseSpecification<CustomField>
    {
        protected CustomFieldsSpecifications(Expression<Func<CustomField, bool>> criteria) 
            : base(criteria)
        {
            AddInclude(e => e.CustomFieldListValues);
        }

        protected CustomFieldsSpecifications(int workSpaceId) 
            : base(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false)
        {
            AddInclude(e => e.CustomFieldListValues);
        }

        protected CustomFieldsSpecifications(int workSpaceId, CustomFieldType fieldType)
            : base(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false
            && e.CustomFieldType == fieldType)
        {
            AddInclude(e => e.CustomFieldListValues);
        }
    }
}
