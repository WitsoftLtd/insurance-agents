﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class ProductsSpecifications : BaseSpecification<Product>
    {
        public ProductsSpecifications(Expression<Func<Product, bool>> criteria) 
            : base(criteria)
        {
            AddInclude(e => e.ProductDetails);
            ApplyOrderBy(e => e.Name);
        }

        public ProductsSpecifications(int workSpaceId) 
            : base(e => e.IsDeleted == false && e.WorkSpaceId == workSpaceId)
        {
            // AddInclude(e => e.ProductDetails);
            // AddInclude("ProductDetails.CustomField");
            ApplyOrderBy(e => e.Name);
        }
    }
}
