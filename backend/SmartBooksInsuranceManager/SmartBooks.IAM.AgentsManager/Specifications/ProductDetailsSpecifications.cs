﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class ProductDetailsSpecifications : BaseSpecification<ProductDetail>
    {
        protected ProductDetailsSpecifications(Expression<Func<ProductDetail, bool>> criteria) 
            : base(criteria)
        {
            AddInclude(e => e.Product);
        }

        protected ProductDetailsSpecifications(int productId)
            : base(e => e.IsDeleted == false && e.ProductId == productId)
        {
            AddInclude(e => e.Product);
        }
    }
}
