﻿using SmartBooks.IAM.Entities.Entities;
using System;
using System.Linq.Expressions;

namespace SmartBooks.IAM.AgentsManager.Specifications
{
    public class ClientsSpecifications : BaseSpecification<Client>
    {
        public ClientsSpecifications(Expression<Func<Client, bool>> criteria) 
            : base(criteria){ }

        public ClientsSpecifications(int workSpaceId)
            : base(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false) { }

        public ClientsSpecifications(int workSpaceId, ClientStatus clientStatus)
            : base(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false
            && e.ClientStatus == clientStatus) { }

        protected ClientsSpecifications(int workSpaceId, ClientType clientType)
           : base(e => e.WorkSpaceId == workSpaceId && e.IsDeleted == false
           && e.ClientType == clientType) { }

    }
}
