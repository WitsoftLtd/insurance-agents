﻿using SmartBooks.IAM.Entities.Entities;
using System.Collections.Generic;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IProduct<T> : IRepository<T>
        where T : Product
    {
        bool NameExists(string name, int workSpaceId);
        bool DuplicateName(int id, string name, int workSpaceId);
        ICollection<Product> Products(int workSpaceId);
    }
}
