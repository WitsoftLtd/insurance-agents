﻿using SmartBooks.IAM.Subscription.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IWorkSpaceMember<T> : IRepository<T>
        where T : WorkSpaceMember
    {
    }
}
