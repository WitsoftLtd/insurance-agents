﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IPolicyProductDetail<T> : IRepository<T>
        where T : PolicyProductDetail
    {
    }
}
