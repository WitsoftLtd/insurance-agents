﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IPayment<T> : IRepository<T>
        where T : Payment
    {
    }
}
