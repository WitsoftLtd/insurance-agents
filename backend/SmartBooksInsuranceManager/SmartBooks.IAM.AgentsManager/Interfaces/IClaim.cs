﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IClaim<T> : IRepository<T>
        where T: Claim
    {
    }
}
