﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IProductDetail<T> : IRepository<T>
        where T : ProductDetail
    {
    }
}
