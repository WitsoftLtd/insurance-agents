﻿using SmartBooks.IAM.Subscription.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IWorkSpace<T> : IRepository<T>
        where T : WorkSpace
    {
    }
}
