﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IClient<T> : IRepository<T>
        where T : Client
    {

    }
}
