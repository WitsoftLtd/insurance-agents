﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IAppointment<T> : IRepository<T>
        where T : Appointment
    {
    }
}
