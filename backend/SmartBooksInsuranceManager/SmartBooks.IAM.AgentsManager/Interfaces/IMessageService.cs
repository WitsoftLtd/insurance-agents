﻿using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IMessageService
    {
        Task<bool> SendEmail(string emailTo, string subject, string body);
        Task<bool> SendSms(string phoneNumber, string message);
    }
}
