﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IFile<T> : IRepository<T>
        where T : File
    {
    }
}
