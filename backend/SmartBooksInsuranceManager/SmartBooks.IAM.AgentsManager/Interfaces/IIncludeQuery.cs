﻿using SmartBooks.IAM.AgentsManager.Helpers.Query;
using System.Collections.Generic;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IIncludeQuery
    {
        Dictionary<IIncludeQuery, string> PathMap { get; }
        IncludeVisitor Visitor { get; }
        HashSet<string> Paths { get; }
    }

    public interface IIncludeQuery<TEntity, out TPreviousProperty> : IIncludeQuery
    {
    }
}
