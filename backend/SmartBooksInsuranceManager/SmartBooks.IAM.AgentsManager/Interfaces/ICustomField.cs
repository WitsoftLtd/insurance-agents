﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface ICustomField<T> : IRepository<T>
        where T : CustomField
    {
    }
}
