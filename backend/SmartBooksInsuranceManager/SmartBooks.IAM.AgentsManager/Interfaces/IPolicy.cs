﻿using SmartBooks.IAM.Entities.Entities;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IPolicy<T> : IRepository<T>
        where T : Policy
    { 
        bool PolicyExists(string number, int workSpaceId);
        bool DuplicatePolicy(int id, string number, int workSpaceId);
        Task<T> GetDetailed(int id);
    }
}
