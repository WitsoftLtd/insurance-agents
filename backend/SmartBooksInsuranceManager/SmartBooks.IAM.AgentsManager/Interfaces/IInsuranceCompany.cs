﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IInsuranceCompany<T> : IRepository<T>
        where T : InsuranceCompany
    {
        bool NameExists(string name, int workSpaceId); 
        bool DuplicateName(int id, string name, int workSpaceId);
    }
}
