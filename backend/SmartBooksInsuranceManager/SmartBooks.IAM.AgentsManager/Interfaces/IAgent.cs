﻿using SmartBooks.IAM.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface IAgent<T> : IRepository<T>
        where T : Agent
    {
        Task<T> GetDetailedUser(int id);
        Task<ICollection<T>> Agents(int workSpaceId);
    }
}
