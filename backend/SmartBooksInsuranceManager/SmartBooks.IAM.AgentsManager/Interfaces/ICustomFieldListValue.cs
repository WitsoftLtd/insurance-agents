﻿using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.AgentsManager.Interfaces
{
    public interface ICustomFieldListValue<T> : IRepository<T>
        where T : CustomFieldListValue
    {
    }
}
