﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Subscription.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }
        [Required]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberBrowser { get; set; }
        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class LoggedInUserViewModel
    {
        public LoggedInUserViewModel()
        {
            WorkSpaces = new List<UserWorkSpaceViewModel>();
        }
        public int Id { get; set; }
        public string Email { get; set; }
        public string TokenString { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public bool Succeeded { get; set; }
        public int DefaultWorkSpace { get; set; }
        public List<UserWorkSpaceViewModel> WorkSpaces { get; set; }
    }

    public class UserWorkSpaceViewModel
    {
        public int Id { get; set; }
        public SubscriptionType SubscriptionType { get; set; }
        public string SubscriptionName
        {
            get
            {
                return SubscriptionType switch
                {
                    SubscriptionType.Trial => "Trial",
                    SubscriptionType.Paying => "Active",
                    SubscriptionType.GracePeriod => "Grace Period",
                    SubscriptionType.Disabled => "Disabled",
                    _ => "",
                };
            }
        }
        public string Name { get; set; }
        public WorkSpaceType WorkSpaceType { get; set; }
        public DateTime ExipryDate { get; set; }
        public UserType UserType { get; set; }
        public string Description { get; set; }
        public bool AutomaticNotifications { get; set; }
        public NotificationType NotificationType { get; set; }
        public int NoofDaysToFirstNotification { get; set; }
        public NotificationFrequency NotificationFrequency { get; set; }
        public string SmsTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public string EmailSubject { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public WorkSpaceType WorkSpaceType { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        public string Code { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}
