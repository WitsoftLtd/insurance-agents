﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartBooks.IAM.Subscription.ViewModels
{
    public class WorkSpaceViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool AutomaticNotifications { get; set; }
        public NotificationType NotificationType { get; set; }
        public int NoofDaysToFirstNotification { get; set; }
        public NotificationFrequency NotificationFrequency { get; set; }
        public string SmsTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public string EmailSubject { get; set; }
    }
}
