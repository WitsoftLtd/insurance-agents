﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Subscription.Entities
{
    public class ApplicationUser : IdentityUser<int>
    {
        public ApplicationUser()
        {
            WorkSpaceMembers = new HashSet<WorkSpaceMember>();
        }
        [MaxLength(150)]
        public string FullName { get; set; }
        [MaxLength(150)]
        public string Address { get; set; }
        [MaxLength(255)]
        public string Details { get; set; }
        public int? DefaultWorkSpace { get; set; }
        public ICollection<WorkSpaceMember> WorkSpaceMembers { get; set; }
    }

    public partial class UserLogin : IdentityUserLogin<int>
    {
        public int Id { get; set; }
    }
    public partial class Role : IdentityRole<int>
    {
        public Role() : base()
        {
        }
        public Role(string roleName)
        {
            Name = roleName;
        }
    }
    public partial class RoleClaim : IdentityRoleClaim<int>
    {
    }
    public partial class UserToken : IdentityUserToken<int>
    {
        public int Id { get; set; }
    }
}
