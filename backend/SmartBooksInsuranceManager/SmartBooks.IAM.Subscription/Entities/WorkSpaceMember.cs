﻿using System;

namespace SmartBooks.IAM.Subscription.Entities
{
    public class WorkSpaceMember : BaseEntity
    {
        public int WorkSpaceId { get; set; }
        public int MemberId { get; set; }
        public DateTime DateJoined { get; set; }
        /// <summary>
        /// Defines user type for ease of implementing default rights
        /// Should be populated at the controller based on the action
        /// i.e CreateAgent, CreateOfficeStaff actions
        /// Shouldn't be at the user level because, user can belong to many
        /// Work-Spaces with different roles.
        /// </summary>
        public UserType UserType { get; set; }
        public int? InvitedById { get; set; }

        public WorkSpace WorkSpace { get; set; }
        public ApplicationUser Member { get; set; }
        public ApplicationUser InvitedBy { get; set; }
    }
}
