﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Subscription.Entities
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public DateTimeOffset TimeCreated { get; set; }
        /// <summary>
        /// To implement soft delete
        /// Because we need this data
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
