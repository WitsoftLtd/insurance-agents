﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Subscription.Entities
{
    public class WorkSpace : BaseEntity
    {
        public WorkSpace()
        {
            WorkSpaceMembers = new HashSet<WorkSpaceMember>();
        }
        [MaxLength(150)]
        public string Name { get; set; }
        /// <summary>
        /// Use to differentiate Individual and Company.
        /// And configure the interface accordingly
        /// </summary>
        public WorkSpaceType WorkSpaceType { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }
        /// <summary>
        /// User who created this Work-space
        /// They are also automatically added to this Work-space members list
        /// </summary>
        public int OwnerId { get; set; }
        public bool AutomaticNotifications { get; set; }
        public NotificationType NotificationType { get; set; }
        public int NoofDaysToFirstNotification { get; set; }
        public NotificationFrequency NotificationFrequency { get; set; }
        [MaxLength(250)]
        public string SmsTemplate { get; set; }
        public string EmailTemplate { get; set; }
        public string EmailSubject { get; set; }

        /// <summary>
        /// Expiry date
        /// Use this for the trial period and replace with a billing implementation,
        /// if the project becomes viable
        /// </summary>
        public DateTime ExpiryDate { get; set; }
        public SubscriptionType SubscriptionType { get; set; }

        public ICollection<WorkSpaceMember> WorkSpaceMembers { get; set; }
    }
}
