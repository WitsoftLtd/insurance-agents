﻿public enum UserType
{
    Admin,
    Agent,
    OfficeStaff,
    Guest
}

/// <summary>
/// Use this to customize user exprience
/// For individual, hide columns like Agents, Office staff, e.t.c
/// </summary>
public enum WorkSpaceType
{
    Individual,
    Company
}

public enum NotificationType
{
    SmsOnly,
    EmailOnly,
    Both,
    None
}

public enum NotificationFrequency
{
    EveryDayFromFirst,
    FirstAndLast,
    FirstLastAndPrevios
}

public enum SubscriptionType
{
    Trial,
    Paying,
    GracePeriod,
    Disabled
}

