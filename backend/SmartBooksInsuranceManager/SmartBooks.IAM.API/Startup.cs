using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Services;
using SmartBooks.IAM.API.Helpers;
using SmartBooks.IAM.Entities.Entities;
using SmartBooks.IAM.Infrastructure.Data;
using SmartBooks.IAM.Infrastructure.Data.Repository;
using SmartBooks.IAM.Infrastructure.Services;
using SmartBooks.IAM.Subscription.Entities;
using System.Text;

namespace SmartBooks.IAM.API
{
    public class Startup
    {
        private readonly string SPAOrigins = "AllowedOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: SPAOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins("http://127.0.0.1:5000",
                                                          "http://localhost:4200",
                                                          "https://localhost:4200",
                                                          "http://agents.witsoft.co.ke",
                                                          "https://agents.witsoft.co.ke",
                                                          "https://agentsjournal.witsoft.co.ke",
                                                          "https://license.witsoft.co.ke",
                                                          "https://www.license.witsoft.co.ke",
                                                          "https://license.waveonweb.co.ke",
                                                          "https://www.license.waveonweb.co.ke",
                                                          "http://www.witsoft.co.ke",
                                                          "https://www.witsoft.co.ke")
                                      .AllowAnyHeader()
                                      .AllowAnyMethod();
                                  });
            });

            //Add context
            services.AddDbContext<InsuranceContext>(options =>
                options.UseMySQL(
                    Configuration.GetConnectionString("DBConnection")));

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            //Use objects that derive from identity TKEY
            services.AddDefaultIdentity<ApplicationUser>(
                options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 1;
                })
                .AddEntityFrameworkStores<InsuranceContext>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = "witsoft.co.ke",
                        ValidAudience = "witsoft.co.ke",
                        IssuerSigningKey = new SymmetricSecurityKey(key)
                    };
                });

            services.AddControllers();

            //Add Message service
            services.AddSingleton<IMessageService, MessageService>();
            services.AddScoped<IAgent<Agent>, AgentsRepository<Agent>>();
            services.AddScoped<IAppointment<Appointment>, AppointmentsRepository<Appointment>>();
            services.AddScoped<IClaim<Claim>, ClaimsRepositoty<Claim>>();
            services.AddScoped<IClient<Client>, ClientsRepository<Client>>();
            services.AddScoped<ICustomField<CustomField>, CustomFieldsRepository<CustomField>>();
            services.AddScoped<ICustomFieldListValue<CustomFieldListValue>, CustomFieldListValueRepository<CustomFieldListValue>>();
            services.AddScoped<IFile<File>, FilesRepository<File>>();
            services.AddScoped<IInsuranceCompany<InsuranceCompany>, InsuranceCompaniesRepository<InsuranceCompany>>();
            services.AddScoped<IPayment<Payment>, PaymentsRepository<Payment>>();
            services.AddScoped<IPolicy<Policy>, PoliciesRepository<Policy>>();
            services.AddScoped<IPolicyProductDetail<PolicyProductDetail>, PolicyProductDetailsRepository<PolicyProductDetail>>();
            services.AddScoped<IProduct<Product>, ProductsRepository<Product>>();
            services.AddScoped<IProductDetail<ProductDetail>, ProductDetailsRepository<ProductDetail>>();
            services.AddScoped<IWorkSpace<WorkSpace>, WorkSpaceRepository<WorkSpace>>();
            services.AddScoped<IWorkSpaceMember<WorkSpaceMember>, WorkSpaceMembersRepository<WorkSpaceMember>>();

            services.AddScoped(typeof(AgentsService));
            services.AddScoped(typeof(AppointmentsService));
            services.AddScoped(typeof(ClaimsService));
            services.AddScoped(typeof(ClientsService));
            services.AddScoped(typeof(CustomFieldsService));
            services.AddScoped(typeof(CustomFieldListValuesService));
            services.AddScoped(typeof(FilesService));
            services.AddScoped(typeof(InsuranceCompaniesService));
            services.AddScoped(typeof(PaymentsService));
            services.AddScoped(typeof(PoliciesService));
            services.AddScoped(typeof(PolicyProductDetailsService));
            services.AddScoped(typeof(ProductDetailsService));
            services.AddScoped(typeof(ProductsService));
            services.AddScoped(typeof(WorkSpaceMemberService));
            services.AddScoped(typeof(WorkSpaceService));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(SPAOrigins);

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
