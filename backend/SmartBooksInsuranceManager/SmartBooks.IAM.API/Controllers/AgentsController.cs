﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Services;
using SmartBooks.IAM.AgentsManager.ViewModels;
using SmartBooks.IAM.Subscription.Entities;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AgentsController : ControllerBase
    {
        private readonly AgentsService _agentService;
        private readonly IMessageService _messageService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly WorkSpaceMemberService _workSpaceMemberService;

        public AgentsController(
            AgentsService agentService,
            IMessageService messageService,
            UserManager<ApplicationUser> userManager,
            WorkSpaceMemberService workSpaceMemberService)
        {
            _userManager = userManager;
            _agentService = agentService;
            _messageService = messageService;
            _workSpaceMemberService = workSpaceMemberService;
        }

        [HttpGet("{workSpaceId}")]
        public async Task<ActionResult> Get(int workSpaceId)
        {
            try
            {
                return Ok(await _agentService.GetAgentsAsync(workSpaceId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Clients/workSpaceId
        [HttpPost("{workSpaceId}")]
        public async Task<ActionResult> Post(int workSpaceId, AgentViewModel model)
        {
            try
            {
                var user = new ApplicationUser
                {
                    Email = model.Email,
                    FullName = model.FullName,
                    UserName = model.Email,
                    PhoneNumber = model.PhoneNumber
                };
                var result = await _userManager.CreateAsync(user, "agent2020");
                if (result.Succeeded)
                {
                    await _workSpaceMemberService.AddAsync(new WorkSpaceMember
                    {
                        DateJoined = DateTime.Now,
                        MemberId = user.Id,
                        UserType = UserType.Admin,
                        WorkSpaceId = workSpaceId,
                        TimeCreated = DateTimeOffset.Now
                    });
                    model.Id = await _agentService.AddAsync(
                        user.Id,
                        DateTimeOffset.Now, 
                        workSpaceId, 
                        model.CanLogIn);
                    return Ok(model);
                }
                return BadRequest("An unknown error occurred, check your inputs and please try again");
            }
            catch (Exception ex)
            {
                return BadRequest("An unknown error occurred, check your inputs and please try again" + ex.Message);
            }

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, AgentViewModel model)
        {
            if (id != model.Id)
            {
                return BadRequest("Invalid request");
            }
            try
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null)
                {
                    return BadRequest("Invalid request");
                }
                user.FullName = model.FullName;
                user.PhoneNumber = model.PhoneNumber;
                await _userManager.UpdateAsync(user);
                await _agentService.UpdateAsync(model.Id, model.CanLogIn);
                return Ok(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var agent = await _agentService.GetDetailedUserAsync(id);
                await _userManager.SetLockoutEnabledAsync(agent.ApplicationUser, true);
                return Ok(await _agentService.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}