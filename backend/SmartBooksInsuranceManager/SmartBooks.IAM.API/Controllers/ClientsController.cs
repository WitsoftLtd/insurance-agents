﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartBooks.IAM.AgentsManager.Services;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClientsController : ControllerBase
    {
        private readonly ClientsService clientsService;
        public ClientsController(ClientsService _clientsService)
        {
            clientsService = _clientsService;
        }

        // GET: api/Clients/workSpaceId
        [HttpGet("{workSpaceId}")]
        public async Task<ActionResult> Get(int workSpaceId)
        {
            try
            {
                return Ok(await clientsService.GetAllAsync(workSpaceId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Clients/workSpaceId
        [HttpPost("{workSpaceId}")]
        public async Task<ActionResult> Post(int workSpaceId, Client viewModel)
        {
            try
            {
                return Ok(await clientsService.AddAsync(viewModel, workSpaceId, DateTimeOffset.Now));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // PUT: api/Clients/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, Client viewModel)
        {
            if (id != viewModel.Id)
            {
                return BadRequest("Invalid request");
            }
            try
            {
                return Ok(await clientsService.UpdateAsync(viewModel));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                return Ok(await clientsService.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}