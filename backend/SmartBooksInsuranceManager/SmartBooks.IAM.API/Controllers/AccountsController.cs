﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.Services;
using SmartBooks.IAM.API.Helpers;
using SmartBooks.IAM.Subscription.Entities;
using SmartBooks.IAM.Subscription.ViewModels;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMessageService _messageService;
        private readonly AgentsService _agentService;
        private readonly WorkSpaceService _workSpaceService;
        private readonly WorkSpaceMemberService _workSpaceMemberService;

        public AccountsController(
            IOptions<AppSettings> appSettings,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IMessageService messageService,
            AgentsService agentService,
            WorkSpaceService workSpaceService,
            WorkSpaceMemberService workSpaceMemberService)
        {
            _appSettings = appSettings.Value;
            _signInManager = signInManager;
            _userManager = userManager;
            _messageService = messageService;
            _agentService = agentService;
            _workSpaceService = workSpaceService;
            _workSpaceMemberService = workSpaceMemberService;
        }

        [HttpPost("Login")]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }

            var user = await _userManager.FindByNameAsync(model.Email);
            if (user != null)
            {
                if (!await _userManager.IsEmailConfirmedAsync(user))
                {
                    //string code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code }, protocol: Request.Scheme);
                    //string html = "Please confirm your account by clicking this : <a href=\"" + callbackUrl + "\">link</a><br/>";
                    //SendEmail(user.Email, "Confirm your account", html);
                    //ModelState.AddModelError("", "Confirm your email");
                    return BadRequest("Confirm your email");
                }
            }
            else
            {
                return BadRequest("Invalid login attempt.");
            }
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, true);
            if (result.Succeeded)
            {
                //Generate token
                var claimsData = new[] { new Claim(ClaimTypes.Name, user.UserName) };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Secret));
                var signingCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

                var tokenString = new JwtSecurityToken(
                     issuer: "witsoft.co.ke",
                     audience: "witsoft.co.ke",
                     expires: DateTime.Now.AddDays(1),
                     claims: claimsData,
                     signingCredentials: signingCred
                    );

                LoggedInUserViewModel loggedInUser = new LoggedInUserViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    FullName = user.FullName,
                    PhoneNumber = user.PhoneNumber,
                    TokenString = new JwtSecurityTokenHandler().WriteToken(tokenString),
                    Succeeded = true,
                    DefaultWorkSpace = user.DefaultWorkSpace ?? 0
                };

                ICollection<WorkSpaceMember> workSpaceMembers =
                    await _workSpaceMemberService.WorkSpaceMembersByUser(user.Id);

                foreach (var item in workSpaceMembers)
                {
                    loggedInUser.WorkSpaces.Add(new UserWorkSpaceViewModel
                    {
                        ExipryDate = item.WorkSpace.ExpiryDate,
                        Id = item.WorkSpace.Id,
                        Name = item.WorkSpace.Name,
                        SubscriptionType = item.WorkSpace.SubscriptionType,
                        UserType = item.UserType,
                        WorkSpaceType = item.WorkSpace.WorkSpaceType,
                        AutomaticNotifications = item.WorkSpace.AutomaticNotifications,
                        Description = item.WorkSpace.Description,
                        EmailTemplate = item.WorkSpace.EmailTemplate,
                        EmailSubject = item.WorkSpace.EmailSubject,
                        NotificationType = item.WorkSpace.NotificationType,
                        SmsTemplate = item.WorkSpace.SmsTemplate,
                        NoofDaysToFirstNotification = item.WorkSpace.NoofDaysToFirstNotification,
                        NotificationFrequency = item.WorkSpace.NotificationFrequency
                    });
                }

                return Ok(loggedInUser);
            }
            if (result.RequiresTwoFactor)
            {
                return BadRequest("You are a miracle worker.");
            }
            if (result.IsLockedOut)
            {
                return BadRequest("Account locked out.");
            }
            else
            {
                return BadRequest("Invalid login attempt.");
            }
        }

        [HttpPost("Register")]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    Email = model.Email,
                    FullName = model.FullName,
                    UserName = model.Email,
                    PhoneNumber = model.PhoneNumber
                };
                try
                {
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {

                        var registeredUser = await _userManager.FindByNameAsync(model.Email);

                        // Generate validation code
                        string code = await _userManager.GenerateEmailConfirmationTokenAsync(registeredUser);

                        result = await _userManager.ConfirmEmailAsync(registeredUser, code);

                        if (result.Succeeded)
                        {
                            WorkSpace workSpace = await _workSpaceService.AddAsync(new WorkSpace
                            {
                                AutomaticNotifications = false,
                                ExpiryDate = DateTime.Now.AddDays(14),
                                Name = string.IsNullOrWhiteSpace(user.FullName) ? user.Email : user.FullName,
                                WorkSpaceType = WorkSpaceType.Individual,
                                OwnerId = registeredUser.Id,
                                TimeCreated = DateTimeOffset.UtcNow
                            });
                            await _workSpaceMemberService.AddAsync(new WorkSpaceMember
                            {
                                DateJoined = DateTime.Now,
                                MemberId = registeredUser.Id,
                                UserType = UserType.Admin,
                                WorkSpaceId = workSpace.Id,
                                TimeCreated = DateTimeOffset.Now
                            });
                            await _agentService.AddAsync(user.Id, DateTimeOffset.Now, workSpace.Id);

                            var claimsData = new[] { new Claim(ClaimTypes.Name, user.UserName) };
                            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Secret));
                            var signingCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

                            var tokenString = new JwtSecurityToken(
                                 issuer: "witsoft.co.ke",
                                 audience: "witsoft.co.ke",
                                 expires: DateTime.Now.AddDays(1),
                                 claims: claimsData,
                                 signingCredentials: signingCred
                                );

                            LoggedInUserViewModel loggedInUser = new LoggedInUserViewModel
                            {
                                Id = registeredUser.Id,
                                Email = user.Email,
                                FullName = user.FullName,
                                PhoneNumber = user.PhoneNumber,
                                TokenString = new JwtSecurityTokenHandler().WriteToken(tokenString),
                                Succeeded = true,
                                DefaultWorkSpace = user.DefaultWorkSpace ?? 0
                            };

                            ICollection<WorkSpaceMember> workSpaceMembers =
                                await _workSpaceMemberService.WorkSpaceMembersByUser(registeredUser.Id);

                            foreach (var item in workSpaceMembers)
                            {
                                loggedInUser.WorkSpaces.Add(new UserWorkSpaceViewModel
                                {
                                    ExipryDate = item.WorkSpace.ExpiryDate,
                                    Id = item.WorkSpace.Id,
                                    Name = item.WorkSpace.Name,
                                    SubscriptionType = item.WorkSpace.SubscriptionType,
                                    UserType = item.UserType,
                                    WorkSpaceType = item.WorkSpace.WorkSpaceType,
                                    AutomaticNotifications = item.WorkSpace.AutomaticNotifications,
                                    Description = item.WorkSpace.Description,
                                    EmailTemplate = item.WorkSpace.EmailTemplate,
                                    EmailSubject = item.WorkSpace.EmailSubject,
                                    NotificationType = item.WorkSpace.NotificationType,
                                    SmsTemplate = item.WorkSpace.SmsTemplate,
                                    NoofDaysToFirstNotification = item.WorkSpace.NoofDaysToFirstNotification,
                                    NotificationFrequency = item.WorkSpace.NotificationFrequency
                                });
                            }

                            return Ok(loggedInUser);
                        }
                        else
                        {
                            return BadRequest(result);
                        }
             
                    }
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }

            }

            // If we got this far, something failed, redisplay form
            return BadRequest(model);
        }
        [HttpGet("ConfirmEmail/{userId}/{code}")]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return BadRequest();
            }
            var user = await _userManager.FindByIdAsync(userId);
            var result = await _userManager.ConfirmEmailAsync(user, code);

            if (result.Succeeded)
            {
                WorkSpace workSpace = await _workSpaceService.AddAsync(new WorkSpace
                {
                    AutomaticNotifications = false,
                    ExpiryDate = DateTime.Now.AddDays(14),
                    Name = string.IsNullOrWhiteSpace(user.FullName) ? user.Email : user.FullName,
                    WorkSpaceType = WorkSpaceType.Individual,
                    OwnerId = user.Id,
                    TimeCreated = DateTimeOffset.UtcNow
                });
                await _workSpaceMemberService.AddAsync(new WorkSpaceMember
                {
                    DateJoined = DateTime.Now,
                    MemberId = user.Id,
                    UserType = UserType.Admin,
                    WorkSpaceId = workSpace.Id,
                    TimeCreated = DateTimeOffset.Now
                });
                await _agentService.AddAsync(user.Id, DateTimeOffset.Now, workSpace.Id);
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost("ForgotPassword")]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return BadRequest();
                }

                //// Send an email with this link
                //string code = await _userManager.GeneratePasswordResetTokenAsync(user);
                //var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Scheme);
                //string html = Properties.Resources.ConfirmEmail;
                //html = html.Replace("{Username}", model.Email);
                //html = html.Replace("{CallbackUrl}", callbackUrl);
                //await _messageService.SendEmail(user.Email, "Confirm your email address", html);

                return Ok(new { model.Email });
            }

            // If we got this far, something failed, redisplay form
            return BadRequest();
        }
        // POST: /Account/ResetPassword ResetPasswordViewModel
        [HttpPost("ResetPassword")]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                return BadRequest();
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Ok();
            }
            return BadRequest(result.ToString());
        }
    }
}