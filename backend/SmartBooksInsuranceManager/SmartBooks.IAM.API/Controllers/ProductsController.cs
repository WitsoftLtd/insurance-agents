﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartBooks.IAM.AgentsManager.Services;
using SmartBooks.IAM.AgentsManager.ViewModels;
using SmartBooks.IAM.Entities.Entities;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductsController : ControllerBase
    {
        private readonly ProductsService productsService;
        public ProductsController(ProductsService _productsService)
        {
            productsService = _productsService;
        }

        // GET: api/Products/workSpaceId
        [HttpGet("{workSpaceId}")]
        public async Task<ActionResult> Get(int workSpaceId)
        {
            try
            {
                return Ok(await productsService.GetAllAsync(workSpaceId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Products/workSpaceId
        [HttpPost("{workSpaceId}")]
        public async Task<ActionResult> Post(int workSpaceId, ProductViewModel viewModel)
        {
            try
            {
                return Ok(await productsService.AddAsync(viewModel, workSpaceId, DateTimeOffset.Now));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, ProductViewModel viewModel)
        {
            if (id != viewModel.Id)
            {
                return BadRequest("Invalid request");
            }
            try
            {
                return Ok(await productsService.UpdateAsync(viewModel));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                return Ok(await productsService.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}