﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartBooks.IAM.AgentsManager.Services;
using SmartBooks.IAM.AgentsManager.ViewModels;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class InsuranceCompaniesController : ControllerBase
    {
        private readonly InsuranceCompaniesService insuranceCompaniesService;
        public InsuranceCompaniesController(InsuranceCompaniesService _insuranceCompaniesService)
        {
            insuranceCompaniesService = _insuranceCompaniesService;
        }
        // GET: api/InsuranceCompanies/workSpaceId
        [HttpGet("{workSpaceId}")]
        public async Task<ActionResult> Get(int workSpaceId)
        {
            try
            {
                return Ok(await insuranceCompaniesService.GetInsuranceCompaniesAsync(workSpaceId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //// GET: api/InsuranceCompanies/GetById/5
        //[HttpGet("{id}", Name = "GetById")]
        //public string GetById(int id)
        //{
        //    return "value";
        //}

        // POST: api/InsuranceCompanies/workSpaceId
        [HttpPost("{workSpaceId}")]
        public async Task<ActionResult> Post(int workSpaceId, CompanyViewModel viewModel)
        {
            try
            {
                return Ok(await insuranceCompaniesService.AddAsync(viewModel, workSpaceId, DateTimeOffset.Now));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }

        // PUT: api/InsuranceCompanies/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, CompanyViewModel viewModel)
        {
            if (id != viewModel.Id)
            {
                return BadRequest("Invalid request");
            }
            try
            {
                return Ok(await insuranceCompaniesService.UpdateAsync(viewModel));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                return Ok(await insuranceCompaniesService.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
