﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartBooks.IAM.Entities.Entities;
using SmartBooks.IAM.Infrastructure.Data;
using SmartBooks.IAM.Subscription.Entities;
using UtilitiesLicensing.Helpers;
using UtilitiesLicensing.Models;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RetailClientsController : ControllerBase
    {
        private readonly InsuranceContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public RetailClientsController(InsuranceContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/MpesaClients
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MpesaClient>>> GetMpesaClients()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            return await _context.MpesaClients
                .Where(e => e.OwnerId == user.Id)
                .ToListAsync();
        }

        // GET: api/MpesaClients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MpesaClient>> GetMpesaClient(int id)
        {
            var mpesaClient = await _context.MpesaClients.FindAsync(id);

            if (mpesaClient == null)
            {
                return NotFound();
            }

            return mpesaClient;
        }

        // PUT: api/MpesaClients/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<ActionResult<MpesaClient>> PutMpesaClient(int id, MpesaClient mpesaClient)
        {
            if (id != mpesaClient.Id)
            {
                return BadRequest();
            }

            

            var thisClient = _context.MpesaClients.Find(mpesaClient.Id);
            TimeSpan timeSpan = thisClient.ExpiryDate.Subtract(DateTime.Now);
            if (timeSpan.TotalDays < 5)
            {
                thisClient.ExpiryDate = DateTime.Now.AddYears(1);
            }
            thisClient.BranchName = mpesaClient.BranchName;
            thisClient.ClientKey = mpesaClient.ClientKey;
            thisClient.ClientName = mpesaClient.ClientName;
            thisClient.LicenseKey = LicenseHelper.GetRetailUtilityLicense(thisClient);

            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            LicenseLog licenseLog = new LicenseLog
            {
                BranchName = thisClient.BranchName,
                ClientKey = thisClient.ClientKey,
                ClientName = thisClient.ClientName,
                CreatedDate = DateTime.Now,
                LicenseKey = thisClient.LicenseKey,
                OwnerId = user.Id
            };
            _context.LicenseLogs.Add(licenseLog);
            _context.Entry(_context.MpesaClients.Find(thisClient.Id)).CurrentValues.SetValues(thisClient);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MpesaClientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest();
                }
            }

            return thisClient;
        }

        // POST: api/MpesaClients
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MpesaClient>> PostMpesaClient(MpesaClient mpesaClient)
        {
            if (string.IsNullOrWhiteSpace(mpesaClient.ClientName))
            {
                return BadRequest("Invalid client name");
            }
            if (string.IsNullOrWhiteSpace(mpesaClient.ClientKey))
            {
                return BadRequest("Invalid client key");
            }
            var user = await _userManager.FindByNameAsync(User.Identity.Name);

            if (_context.MpesaClients.Any(e => e.ClientName == mpesaClient.ClientName
                && e.BranchName == mpesaClient.BranchName))
            {
                return BadRequest(new { new Exception("Client already exist").Message });
            }

            mpesaClient.CreatedDate = DateTime.Now;
            mpesaClient.ExpiryDate = DateTime.Now.AddYears(1);
            mpesaClient.LicenseKey = LicenseHelper.GetRetailUtilityLicense(mpesaClient);
            mpesaClient.OwnerId = user.Id;

            LicenseLog licenseLog = new LicenseLog
            {
                BranchName = mpesaClient.BranchName,
                ClientKey = mpesaClient.ClientKey,
                ClientName = mpesaClient.ClientName,
                CreatedDate = DateTime.Now,
                LicenseKey = mpesaClient.LicenseKey,
                OwnerId = user.Id
            };

            _context.LicenseLogs.Add(licenseLog);
            _context.MpesaClients.Add(mpesaClient);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMpesaClient", new { id = mpesaClient.Id }, mpesaClient);
        }

        // DELETE: api/MpesaClients/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MpesaClient>> DeleteMpesaClient(int id)
        {
            var mpesaClient = await _context.MpesaClients.FindAsync(id);
            if (mpesaClient == null)
            {
                return NotFound();
            }

            //_context.MpesaClients.Remove(mpesaClient);
            //await _context.SaveChangesAsync();

            return mpesaClient;
        }

        private bool MpesaClientExists(int id)
        {
            return _context.MpesaClients.Any(e => e.Id == id);
        }
    }
}