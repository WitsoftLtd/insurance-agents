﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartBooks.IAM.AgentsManager.Interfaces;
using SmartBooks.IAM.AgentsManager.ViewModels;
using SmartBooks.IAM.Subscription.ViewModels;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkSpacesController : ControllerBase
    {
        private readonly IMessageService messageService;
        public WorkSpacesController(IMessageService _messageService)
        {
            messageService = _messageService;
        }
        [HttpPost("TestSms/{workSpaceId}")]
        public async Task<ActionResult> Post(int workSpaceId, SmsModel viewModel)
        {
            return Ok(await messageService.SendSms(viewModel.PhoneNumber, viewModel.Message));
        }
        [HttpPost("SendTestEmail/{workSpaceId}")]
        public async Task<ActionResult> SendTestEmail(int workSpaceId, EmailModel viewModel)
        {
            return Ok(await messageService.SendEmail(viewModel.Email, viewModel.EmailSubject, viewModel.EmailMessage));
        }

        [HttpPut("{workSpaceId}")]
        public async Task<ActionResult> Put(WorkSpaceViewModel viewModel)
        {
            return null;
        }
    }
}