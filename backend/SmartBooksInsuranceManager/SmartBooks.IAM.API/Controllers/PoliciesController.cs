﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SmartBooks.IAM.AgentsManager.Services;
using SmartBooks.IAM.AgentsManager.ViewModels;

namespace SmartBooks.IAM.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PoliciesController : ControllerBase
    {
        private readonly PoliciesService policiesService;
        public PoliciesController(PoliciesService _policiesService)
        {
            policiesService = _policiesService;
        }

        // GET: api/Products/workSpaceId
        [HttpGet("{workSpaceId}")]
        public async Task<ActionResult> Get(int workSpaceId)
        {
            try
            {
                return Ok(await policiesService.GetAllAsync(workSpaceId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Products/workSpaceId
        [HttpPost("{workSpaceId}")]
        public async Task<ActionResult> Post(int workSpaceId, PolicyViewModel viewModel)
        {
            try
            {
                return Ok(await policiesService.AddAsync(viewModel, workSpaceId, DateTimeOffset.Now));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, PolicyViewModel viewModel)
        {
            if (id != viewModel.Id)
            {
                return BadRequest("Invalid request");
            }
            try
            {
                return Ok(await policiesService.UpdateAsync(viewModel));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                return Ok(await policiesService.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}