﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilitiesLicensing.Models
{
    public class MpesaClient
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string BranchName { get; set; }
        /// <summary>
        /// Motherboard serial number
        /// </summary>
        public string ClientKey { get; set; }
        public string LicenseKey { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int OwnerId { get; set; }
    }
}
