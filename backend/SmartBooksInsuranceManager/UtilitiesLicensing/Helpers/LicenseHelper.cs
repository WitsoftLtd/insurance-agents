﻿using System;
using System.Security.Cryptography;
using System.Text;
using UtilitiesLicensing.Models;

namespace UtilitiesLicensing.Helpers
{
    public static class LicenseHelper
    {
        public static string GetMpesaUtilityLicense(this MpesaClient client)
        {
            string hashedText = GetHash($"{client.BranchName}{client.ClientKey}{client.ClientName}RemusjuniorDolicaNeema");
            return $"{hashedText.Substring(0, 12)}{EncryptDate(client.ExpiryDate)}";
        }

        public static string GetRetailUtilityLicense(this MpesaClient client)
        {
            string hashedText = GetHash($"{client.BranchName}{client.ClientKey}{client.ClientName}RemusMuthomijuniorDolicaNeema");
            return $"{hashedText.Substring(0, 12)}{EncryptDate(client.ExpiryDate)}";
        }

        private static string EncryptDate(DateTime dateTime)
        {
            return $"{EncryptYear(dateTime.Year - 2000)}{EncryptDay(dateTime.Day)}{EncryptMonth(dateTime.Month)}";
        }

        private static string GetHash(string plainText)
        {
            using (var hashEngine = new MD5CryptoServiceProvider())
            {
                return BitConverter.ToString(
                    hashEngine.ComputeHash(
                        Encoding.UTF8.GetBytes(plainText)
                        )).Replace("-", "");
            }
        }

        private static string EncryptYear(int year)
        {
            switch (year)
            {
                case 20:
                    return "Z";
                case 21:
                    return "M";
                case 22:
                    return "B";
                case 23:
                    return "P";
                case 24:
                    return "Y";
                case 25:
                    return "L";
                case 26:
                    return "C";
                case 27:
                    return "Q";
                case 28:
                    return "X";
                case 29:
                    return "K";
                case 30:
                    return "D";
                default:
                    return "Z";
            }
        }
        private static string EncryptMonth(int MyMonth)
        {
            switch (MyMonth)
            {
                case 1:
                    return "K";
                case 2:
                    return "W";
                case 3:
                    return "T";
                case 4:
                    return "R";
                case 5:
                    return "N";
                case 6:
                    return "A";
                case 7:
                    return "M";
                case 8:
                    return "C";
                case 9:
                    return "J";
                case 10:
                    return "D";
                case 11:
                    return "L";
                case 12:
                    return "S";
                default:
                    return "A";
            }
        }
        private static int EncryptDay(int MyDay)
        {
            switch (MyDay)
            {
                case 1:
                    return 42;
                case 2:
                    return 93;
                case 3:
                    return 44;
                case 4:
                    return 91;
                case 5:
                    return 46;
                case 6:
                    return 89;
                case 7:
                    return 48;
                case 8:
                    return 87;
                case 9:
                    return 50;
                case 10:
                    return 85;
                case 11:
                    return 52;
                case 12:
                    return 83;
                case 13:
                    return 54;
                case 14:
                    return 81;
                case 15:
                    return 56;
                case 16:
                    return 79;
                case 17:
                    return 58;
                case 18:
                    return 77;
                case 19:
                    return 60;
                case 20:
                    return 75;
                case 21:
                    return 62;
                case 22:
                    return 73;
                case 23:
                    return 64;
                case 24:
                    return 71;
                case 25:
                    return 66;
                case 26:
                    return 69;
                case 27:
                    return 68;
                case 28:
                    return 67;
                case 29:
                    return 70;
                case 30:
                    return 65;
                case 31:
                    return 72;
                default:
                    return 71;
            }
        }
    }
}
