using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UtilitiesLicensing.Helpers;

namespace SmartBooks.IAM.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string lice = LicenseHelper.GetRetailUtilityLicense(new UtilitiesLicensing.Models.MpesaClient
            {
                BranchName = "HQ",
                ClientKey = "161CPCCKB00QE1NWTT",
                ClientName = "WITSOFT",
                ExpiryDate = DateTime.Now.AddYears(1)
            });
            Assert.AreEqual(lice, "566y");
        }
    }

}
