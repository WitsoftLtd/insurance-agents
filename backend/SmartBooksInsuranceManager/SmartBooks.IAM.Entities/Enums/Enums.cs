﻿
public enum ClientType
{
    Individual,
    Company
}

public enum ClientStatus
{
    Client = 1,
    Prospect = 2,
    FormerClients = 3,
}

/// <summary>
/// Replace with an entity in the database for ease of multi language implementation
/// After proving project viability
/// </summary>
public enum Gender
{
    Female,
    Male,
    Other
}

/// <summary>
/// Replace with an entity in the database for ease of multi language implementation
/// After proving project viability
/// </summary>
public enum MaritalStatus
{
    Single,
    Married,
    DomesticPartnership,
    Divorced,
    Widowed
}

public enum PolicyStatus
{
    PreActive,
    Active,
    History,
    Canceled
}

public enum CustomFieldDataType
{
    String = 1,
    Memo = 2,
    Numeric = 3,
    Date = 4,
    List = 5
}

public enum CustomFieldType
{
    Client,
    Product,
    Policy
}

public enum PaymentMethod
{
    Cash,
    MobileMoney,
    Debit_Credit_Card,
    StandingOrder,
    Payroll,
    Other
}

public enum PaymentFrequency
{
    Monthly,
    Bi_Monthly,
    Quartely,
    Half_Yearly,
    Annual
}

public enum ClaimStatus
{
    InProgress,
    Fulfilled
}