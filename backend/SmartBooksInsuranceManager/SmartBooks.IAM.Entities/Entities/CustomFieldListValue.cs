﻿using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Entities.Entities
{
    public class CustomFieldListValue : BaseEntity
    {
        public int CustomFieldId { get; set; }
        [MaxLength(150)]
        public string Value { get; set; }

        public CustomField CustomField { get; set; }
    }
}
