﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartBooks.IAM.Entities.Entities
{
    public class LicenseLog
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string BranchName { get; set; }
        public string ClientKey { get; set; }
        public string LicenseKey { get; set; }
        public DateTime CreatedDate { get; set; }
        public int OwnerId { get; set; }
    }
}
