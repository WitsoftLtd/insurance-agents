﻿using SmartBooks.IAM.Subscription.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartBooks.IAM.Entities.Entities
{
    public class Policy : BaseEntity
    {
        public Policy()
        {
            PolicyProductDetails = new HashSet<PolicyProductDetail>();
            Claims = new HashSet<Claim>();
            Payments = new HashSet<Payment>();
        }
        public int ClientId { get; set; }
        public int InsuranceCompanyId { get; set; }
        public int ProductId { get; set; }
        /// <summary>
        /// Hide for individual workspaces
        /// And pick the user as agent
        /// </summary>
        public int AgentId { get; set; }
        [MaxLength(150)]
        public string PolicyNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        /// <summary>
        /// In months
        /// ReadOnly field, difference between effective date and expirydate
        /// </summary>
        public int TermLength { get; set; }
        public decimal InsuredSummary { get; set; }
        public decimal Premium { get; set; }
        /// <summary>
        /// Amount policy holder as to pay before insurance company can pay anything
        /// Even if something were to happen before total premium paid is equal or greater
        /// than deductible, you don't get anything
        /// </summary>
        public decimal Deductible { get; set; }
        public decimal SalesCommission { get; set; }
        public PolicyStatus PolicyStatus { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

        //Readonly fields
        public decimal TotalCharged { get; set; }
        public decimal AdvancePayment { get; set; }
        [NotMapped]
        public decimal Balance
        {
            get
            {
                return TotalCharged - AdvancePayment;
            }
        }

        public Client Client { get; set; }
        public InsuranceCompany InsuranceCompany { get; set; }
        public Product Product { get; set; }
        public Agent Agent { get; set; }

        public ICollection<PolicyProductDetail> PolicyProductDetails { get; set; }
        public ICollection<Claim> Claims { get; set; }
        public ICollection<Payment> Payments { get; set; }
    }
}
