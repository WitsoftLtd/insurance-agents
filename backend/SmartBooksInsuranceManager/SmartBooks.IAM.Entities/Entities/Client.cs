﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SmartBooks.IAM.Entities.Entities
{
    public class Client : BaseEntity
    {
        public ClientType ClientType { get; set; }
        public ClientStatus ClientStatus { get; set; }
        /// <summary>
        /// Use for both company and individual
        /// </summary>
        [MaxLength(150)]
        public string ClientName { get; set; }
        [MaxLength(150)]
        public string ClientNumber { get; set; }
        [MaxLength(150)]
        public string Email { get; set; }
        [MaxLength(150)]
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Alternative phone number
        /// </summary>
        [MaxLength(150)]
        public string AltPhoneNumber { get; set; }
        [MaxLength(150)]
        public string Address { get; set; }
        [MaxLength(150)]
        public string PIN { get; set; }
        [MaxLength(255)]
        public string Details { get; set; }
        //Company
        [MaxLength(150)]
        public string ContactPersonName { get; set; }
        [MaxLength(150)]
        public string ContactPersonPhone { get; set; }
        //Individual
        public string NationalId { get; set; }
        public DateTime? DateofBirth { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        [MaxLength(150)]
        public string EmergencyContactName { get; set; }
        [MaxLength(150)]
        public string EmergencyContactPhone { get; set; }

    }
}
