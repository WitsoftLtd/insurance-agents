﻿using System.Collections.Generic;

namespace SmartBooks.IAM.Entities.Entities
{
    public class Product : BaseEntity
    {
        public Product()
        {
            ProductDetails = new HashSet<ProductDetail>();
        }
        public string Name { get; set; }

        public ICollection<ProductDetail> ProductDetails { get; set; }

    }
}
