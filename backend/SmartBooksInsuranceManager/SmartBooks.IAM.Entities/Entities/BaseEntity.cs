﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Entities.Entities
{
    public class BaseEntity: Subscription.Entities.BaseEntity
    {
        public int WorkSpaceId { get; set; }
    }
}
