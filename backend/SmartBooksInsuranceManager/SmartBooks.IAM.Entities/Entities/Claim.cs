﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Entities.Entities
{
    public class Claim : BaseEntity
    {
        public int PolicyId { get; set; }
        public DateTime ClaimDate { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountPaid { get; set; }
        public ClaimStatus ClaimStatus { get; set; }
        public DateTime? FulfillmentDate { get; set; }
        [MaxLength(255)]
        public string Memo { get; set; }
        public Policy Policy { get; set; }
    }
}
