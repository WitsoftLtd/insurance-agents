﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Entities.Entities
{
    public class CustomField : BaseEntity
    {
        public CustomField()
        {
            CustomFieldListValues = new HashSet<CustomFieldListValue>();
        }
        [MaxLength(150)]
        public string Label { get; set; }
        public CustomFieldType CustomFieldType { get; set; }
        public CustomFieldDataType DataType { get; set; }
        public bool Required { get; set; }
        [MaxLength(255)]
        public string DefaultValue { get; set; }

        public ICollection<CustomFieldListValue> CustomFieldListValues { get; set; }
    }
}
