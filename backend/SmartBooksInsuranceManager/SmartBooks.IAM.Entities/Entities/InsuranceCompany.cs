﻿using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Entities.Entities
{
    public class InsuranceCompany : BaseEntity
    {
        [MaxLength(150)]
        public string Name { get; set; }
        [MaxLength(150)]
        public string ContactPerson { get; set; }
        [MaxLength(150)]
        public string ContactPersonPhone { get; set; }
        [MaxLength(150)]
        public string Email { get; set; }
        [MaxLength(150)]
        public string Address { get; set; }
        [MaxLength(150)]
        public string WebAddress { get; set; }
        [MaxLength(255)]
        public string Details { get; set; }
    }
}
