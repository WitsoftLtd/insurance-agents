﻿using SmartBooks.IAM.Subscription.Entities;

namespace SmartBooks.IAM.Entities.Entities
{
    public class Agent: BaseEntity
    {
        public bool CanLogIn { get; set; }
        public int ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
