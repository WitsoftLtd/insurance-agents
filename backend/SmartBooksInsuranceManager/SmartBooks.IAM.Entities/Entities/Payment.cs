﻿using System;

namespace SmartBooks.IAM.Entities.Entities
{
    public class Payment : BaseEntity
    {
        public int PolicyId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }


        public Policy Policy { get; set; }
    }
}
