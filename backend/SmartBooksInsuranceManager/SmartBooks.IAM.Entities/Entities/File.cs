﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SmartBooks.IAM.Entities.Entities
{
    public class File : BaseEntity
    {
        [MaxLength(150)]
        public string OriginalName { get; set; }
        [MaxLength(255)]
        public string Path { get; set; }
        [MaxLength(150)]
        public string Extension { get; set; }
    }
}
