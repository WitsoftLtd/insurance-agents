﻿using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Entities.Entities
{
    public class PolicyProductDetail : BaseEntity
    {
        public int PolicyId { get; set; }
        public int ProductDetailId { get; set; }
        [MaxLength(255)]
        public string Value { get; set; }

        public Policy Policy { get; set; }
        public ProductDetail ProductDetail { get; set; }
    }
}
