﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartBooks.IAM.Entities.Entities
{
    public class Appointment : BaseEntity
    {
        public int AgentId { get; set; }
        public int? ClientId { get; set; }
        public DateTime AppointmentTime { get; set; }
        [MaxLength(255)]
        public string Details { get; set; }
        public Agent Agent { get; set; }
        public Client Client { get; set; }
    }
}
