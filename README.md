# README #
Insurance Agent is a virtual assistant for Insurance professionals. It handles all the information needed for developing a strong Insurance database. Keep up to date detailed info about your clients, products (Insurance policies), Insurance Companies (Carriers) and important Contacts.

Nowadays, Agents spend considerable time developing and pursuing sales leads. Automating the insurance lifecycle you will reduce the amount of time that you spend on time consuming tasks (your workload), which will improve your efficiency and allow you to concentrate on increasing production. Now you can focus on what matters most... building a profitable and competitive business!

This app provides an intuitive interface to help you accomplish your most common task effectively.

Key Features:
- Manage Insurance Companies (carriers) and Contacts.
- Manage several Insurance Agents simultaneously. This is really useful for small agencies.
- Keep a complete record of your Clients and Prospects. We provide an effective search capability on every module of this app.
- Record your Appointments.
- Create Insurance Policies. Support for several types of Insurances (Property / Casualty, Home, Auto, Health, Dental, Life, etc). Fully manage Claims and Payments. Attach up to 10 images.
- Multiple Statistics available. Beautiful Charts with detailed data for precise, effective, quick analysis of your sales performance.
- Quick reports are available for commissions and premiums. There are several filters available (Company, Agent, Periods, etc)
- You can get quick reports including clients, commissions and sales filtered by companies, dates, etc.
- The User Interface is very intuitive and optimized for tablets.
- Provides powerful and effective set of features that will keep growing in future updates.

PRO Features (Available via In-App Purchase):
- Access to Images in Policies and Claims
- Access to Products (templates)
- AD-FREE (No Advertisements).

  <h1>Pay for value and not technology</h1>
  <p>
    Cloud computing certainly democratizes access to cutting-edge technology. By lowering the cost to invest and prioritizing remote access to apps and data, users (and businesses) are gaining level footing with larger enterprise corporations. In other words, you don’t have to have a million dollar IT department to have a voice or a technological edge.
  </p>
  <p>
    Value is the key metric by which technology will rise or fall. Customers just aren’t impressed by bells and flashy lights. It’s no longer cool to have a popular song for your ringtone — what’s cool is your ability to use one device to talk with clients across the country one moment, then preview a document on it the next.
  </p>
  <p>
    “If someone asks me what cloud computing is, I try not to get bogged down with definitions. I tell them that, simply put, cloud computing is a better way to run your business.” ~ Marc Benioff, Founder, CEO and Chairman of Salesforce
  </p>
  <p>
    “Line-of-business leaders everywhere are bypassing IT departments to get applications from the cloud (also known as software as a service, or SaaS) and paying for them like they would a magazine subscription. And when the service is no longer required, they can cancel that subscription with no equipment left unused in the corner.” ~ Daryl Plummer, Managing Vice President and Distinguished Analyst at Gartner
  </p>
  <p>
    “Cloud computing is empowering, as anyone in any part of world with internet connection and a credit card can run and manage applications in the state of the art global datacenters; companies leveraging cloud will be able to innovate cheaper and faster.” ~ Jamal Mazhar, Founder and CEO of Kaavo
  </p>